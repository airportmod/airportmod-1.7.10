package airportlight.radar;

public enum FlightType {
    IFR, VFR
}
