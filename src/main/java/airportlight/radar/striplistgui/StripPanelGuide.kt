package airportlight.radar.striplistgui

import airportlight.radar.guiparts.CustomGridBagConstraints
import java.awt.Color
import java.awt.Dimension
import java.awt.GridBagLayout
import javax.swing.*
import javax.swing.border.LineBorder

class StripPanelGuide : JPanel() {

    private val callSignJLabel: JLabel

    /**
     * ボーダーを持ったコンポーネントリスト
     */
    private val haveBorder = ArrayList<JComponent>()

    init {
        //レイアウトのベースを作成
        val gbLayout = GridBagLayout()
        this.layout = gbLayout

        //各情報表示のコンポーネントを作成
        val l1 = JLabel("CallSign", JTextField.CENTER, Dimension(45, 20))
        callSignJLabel = l1
        val l25 = JLabel("aircraft Type/Equipment", SwingConstants.CENTER, Dimension(45, 20))
        val l3 = JLabel("squawk", SwingConstants.CENTER, Dimension(42, 20))//スコーク
        val l4 = JLabel("cruiseSpeed", SwingConstants.CENTER, Dimension(42, 20))
        val l6 = JLabel("instructedAlt", SwingConstants.CENTER, Dimension(25, 20))
        val l7 = JLabel("tempAlt", SwingConstants.CENTER, Dimension(25, 20))
        val l81 = JLabel("fixName", SwingConstants.CENTER, Dimension(75, 20))
        val l82 = JLabel("fixTime", SwingConstants.CENTER, Dimension(75, 20))
        val l9 = JLabel("Route", SwingConstants.CENTER, Dimension(182, 38))
        val lInstructedSpeed = JLabel("instructedSpeed", JTextField.CENTER, Dimension(32, 20))
        val l10 = JLabel("instructedHeading", JTextField.CENTER, Dimension(32, 20))
        val l11 = JLabel("memoLong", SwingConstants.CENTER, Dimension(150, 40))
        val l12 = JLabel("arrivalAirport", JTextField.CENTER, Dimension(50, 40))

        //ボーダーを持つものをリストに追加
        haveBorder.add(l1)
        haveBorder.add(l25)
        haveBorder.add(l3)
        haveBorder.add(l4)
        haveBorder.add(l6)
        haveBorder.add(l7)
        haveBorder.add(l81)
        haveBorder.add(l82)
        haveBorder.add(l9)
        haveBorder.add(lInstructedSpeed)
        haveBorder.add(l10)
        haveBorder.add(l11)
        haveBorder.add(l12)

        //ATCTypeに合わせて色合いを設定
        updateColorTheme()

        //コンポーネントをグリッドレイアウトに配置
        val gbc = CustomGridBagConstraints(0, 0, 2, 1, 2.7)
        gbLayout.setConstraints(l1, gbc)
        this.add(l1)

        gbc.update(0, 1, 2, 1, 2.7)
        gbLayout.setConstraints(l25, gbc)
        this.add(l25)

        gbc.update(0, 2, 1, 1, 1.7)
        gbLayout.setConstraints(l3, gbc)
        this.add(l3)

        gbc.update(0, 3, 1, 1, 1.7)
        gbLayout.setConstraints(l4, gbc)
        this.add(l4)

        gbc.update(1, 3, 1, 1, 1.0)
        val blanc13 = JLabel("", SwingConstants.CENTER)
        blanc13.preferredSize = Dimension(25, 20)
        gbLayout.setConstraints(blanc13, gbc)
        this.add(blanc13)

        gbc.update(2, 0, 1, 1, 1.0)
        gbLayout.setConstraints(l6, gbc)
        this.add(l6)

        gbc.update(2, 1, 1, 1, 1.0)
        gbLayout.setConstraints(l7, gbc)
        this.add(l7)

        gbc.update(3, 0, 1, 1, 3.0)
        gbLayout.setConstraints(l81, gbc)
        this.add(l81)

        gbc.update(3, 1, 1, 1, 3.0)
        gbLayout.setConstraints(l82, gbc)
        this.add(l82)

        gbc.update(3, 3, 1, 1, 3.0)
        gbLayout.setConstraints(lInstructedSpeed, gbc)
        this.add(lInstructedSpeed)

        gbc.update(4, 0, 2, 2, 7.3)
        gbLayout.setConstraints(l9, gbc)
        this.add(l9)

        gbc.update(4, 2, 1, 1, 1.3)
        val blanc43 = JLabel("", SwingConstants.CENTER)
        blanc13.preferredSize = Dimension(32, 20)
        gbLayout.setConstraints(blanc43, gbc)
        this.add(blanc43)

        gbc.update(4, 3, 1, 1, 1.3)
        gbLayout.setConstraints(l10, gbc)
        this.add(l10)

        gbc.update(5, 2, 1, 2, 6.0)
        gbLayout.setConstraints(l11, gbc)
        this.add(l11)

        gbc.update(6, 0, 1, 2, 2.0)
        gbLayout.setConstraints(l12, gbc)
        this.add(l12)
    }

    /**
     * ATCTypeに基づいてカラーリングを反映
     */
    private fun updateColorTheme() {
        val (borderColor, backGroundColor) = Pair(Color(0, 0, 0), Color(200, 200, 200))
        this.border = LineBorder(borderColor, 2)
        this.background = backGroundColor

        for (component in this.haveBorder) {
            component.border = LineBorder(borderColor, 2)
        }
        callSignJLabel.background = borderColor
    }

    private fun JLabel(text: String?, horizontalAlignment: Int, dimension: Dimension): JLabel {
        return JLabel(text, horizontalAlignment).also { preferredSize = dimension }
    }
}