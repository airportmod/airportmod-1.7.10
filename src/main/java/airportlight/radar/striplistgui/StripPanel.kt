package airportlight.radar.striplistgui

import airportlight.modcore.PacketHandlerAPM
import airportlight.radar.ATCType
import airportlight.radar.StripData
import airportlight.radar.StripDataType
import airportlight.radar.artsdisplay.ArtsDisplayDataSync
import airportlight.radar.guiparts.CustomGridBagConstraints
import airportlight.radar.guiparts.jswingparts.IntegerLimitDocumentFilter
import airportlight.radar.system.syncmessage.StripDataWriteSync
import java.awt.Color
import java.awt.Dimension
import java.awt.GridBagLayout
import java.awt.Insets
import java.awt.event.ActionEvent
import javax.swing.*
import javax.swing.border.LineBorder
import javax.swing.text.AbstractDocument
import javax.swing.text.JTextComponent

class StripPanel(private val stripListGui: StripListGui, val stripData: StripData) : JPanel() {
    /**
     * StripDataTypeとコンポーネントの対応表
     */
    private var typeJComponentMap = HashMap<StripDataType, Pair<JTextComponent?, InfoUpdate>>()
    private val callSignJTextField: JTextField

    /**
     * ボーダーを持ったコンポーネントリスト
     */
    private val haveBorder = ArrayList<JComponent>()

    init {
        //レイアウトのベースを作成
        val gbLayout = GridBagLayout()
        this.layout = gbLayout
        this.minimumSize = Dimension(400, 120)

        //各情報表示のコンポーネントを作成
        val l1 = JTextFieldWithMapSet(StripDataType.CallSign, stripData.callSign, JTextField.CENTER, Dimension(45, 20))
        l1.addActionListener(this::updateCallSign)
        callSignJTextField = l1
        val l25 = JLabel(stripData.aircraftType + stripData.aircraftEquipment, SwingConstants.CENTER, Dimension(45, 20))
        val l3 = JLabel("S" + stripData.squawk, SwingConstants.CENTER, Dimension(42, 20))//スコーク
        val l4 = JLabel(String.format("%03d", stripData.cruiseSpeed), SwingConstants.CENTER, Dimension(42, 20))
        val l6 = JTextFieldWithMapSet(
            StripDataType.InstructedAltitude,
            String.format("%03d", stripData.instructedAltitude),
            JTextField.CENTER,
            Dimension(25, 20),
            true,
            3
        )
        l6.addActionListener(this::updateInstructedAlt)
        val l7 = JTextFieldWithMapSet(
            StripDataType.TempAltitude,
            String.format("%03d", stripData.tempAltitude),
            JTextField.CENTER,
            Dimension(25, 20),
            true,
            3
        )
        l7.addActionListener(this::updateTempAlt)
        val l81 = JLabel("-----", SwingConstants.RIGHT, Dimension(75, 20))
        val l82 = JLabel("--:--", SwingConstants.RIGHT, Dimension(75, 20))
        val l9 = JTextArea(stripData.route, 3, 23)
        l9.lineWrap = true
        l9.wrapStyleWord = true
        l9.preferredSize = Dimension(182, 38)
        typeJComponentMap[StripDataType.Route] = Pair(l9, object : InfoUpdate {
            override fun update() {
                l9.text = stripData.route
                l9.caretPosition = l9.document.length
            }
        })
        l9.addCaretListener {
            if (l9.text != stripData.route) {
                stripListGui.requestSend(
                    StripDataWriteSync.syncRoute(stripData.entityID, l9.text)
                )
            }
        }
        val lInstructedSpeed =
            JTextFieldWithMapSet(
                StripDataType.InstructedSpeed,
                String.format("%03d", stripData.instructedSpeed),
                JTextField.CENTER,
                Dimension(32, 20),
                true,
                3
            )
        lInstructedSpeed.addActionListener(this::updateInstructedSpeed)
        val l10 = JTextFieldWithMapSet(
            StripDataType.InstructedHeading,
            stripData.instructedHeading,
            JTextField.CENTER,
            Dimension(32, 20),
            false,
            5
        )
        l10.addActionListener(this::updateInstructedHeading)
        val l11 = JTextArea(stripData.memoLong)
        l11.preferredSize = Dimension(150, 40)
        typeJComponentMap[StripDataType.MemoLong] = Pair(l11, object : InfoUpdate {
            override fun update() {
                l11.text = stripData.memoLong
            }
        })
        l11.addCaretListener {
            if (l11.text != stripData.memoLong) {
                stripListGui.requestSend(
                    StripDataWriteSync.syncMemoLong(stripData.entityID, l11.text)
                )
            }
        }
        val l12 = JTextFieldWithMapSet(
            StripDataType.ArrivalAirport,
            stripData.arrivalAirport,
            JTextField.CENTER,
            Dimension(50, 40),
            false,
            4
        )
        l12.columns = 4
        l12.addActionListener(this::updateArrivalAirport)

        val bu = JButton("▲")
        bu.horizontalAlignment = JTextField.CENTER
        bu.preferredSize = Dimension(20, 20)
        bu.margin = Insets(0, 0, 0, 0)
        bu.isEnabled = false

        val bd = JButton("▼")
        bd.horizontalAlignment = JTextField.CENTER
        bd.preferredSize = Dimension(20, 20)
        bd.margin = Insets(0, 0, 0, 0)
        bd.isEnabled = false

        val bInfo = JButton("…")
        bInfo.horizontalAlignment = JTextField.CENTER
        bInfo.preferredSize = Dimension(20, 20)
        bInfo.margin = Insets(0, 0, 0, 0)
        bInfo.isEnabled = false

        val bDelete = JButton("☓")
        bDelete.horizontalAlignment = JTextField.CENTER
        bDelete.preferredSize = Dimension(20, 20)
        bDelete.margin = Insets(0, 0, 0, 0)
        bDelete.addActionListener { deleteStrip() }

        //ボーダーを持つものをリストに追加
        haveBorder.add(l1)
        haveBorder.add(l25)
        haveBorder.add(l3)
        haveBorder.add(l4)
        haveBorder.add(l6)
        haveBorder.add(l7)
        haveBorder.add(l81)
        haveBorder.add(l82)
        haveBorder.add(l9)
        haveBorder.add(lInstructedSpeed)
        haveBorder.add(l10)
        haveBorder.add(l11)
        haveBorder.add(l12)

        //ATCTypeに合わせて色合いを設定
        updateColorTheme()

        //コンポーネントをグリッドレイアウトに配置
        val gbc = CustomGridBagConstraints(0, 0, 2, 1, 2.7)
        gbLayout.setConstraints(l1, gbc)
        this.add(l1)

        gbc.update(0, 1, 2, 1, 2.7)
        gbLayout.setConstraints(l25, gbc)
        this.add(l25)

        gbc.update(0, 2, 1, 1, 1.7)
        gbLayout.setConstraints(l3, gbc)
        this.add(l3)

        gbc.update(0, 3, 1, 1, 1.7)
        gbLayout.setConstraints(l4, gbc)
        this.add(l4)

        gbc.update(1, 3, 1, 1, 1.0)
        val blanc13 = JLabel("", SwingConstants.CENTER)
        blanc13.preferredSize = Dimension(25, 20)
        gbLayout.setConstraints(blanc13, gbc)
        this.add(blanc13)

        gbc.update(2, 0, 1, 1, 1.0)
        gbLayout.setConstraints(l6, gbc)
        this.add(l6)

        gbc.update(2, 1, 1, 1, 1.0)
        gbLayout.setConstraints(l7, gbc)
        this.add(l7)

        gbc.update(3, 0, 1, 1, 3.0)
        gbLayout.setConstraints(l81, gbc)
        this.add(l81)

        gbc.update(3, 1, 1, 1, 3.0)
        gbLayout.setConstraints(l82, gbc)
        this.add(l82)

        gbc.update(3, 3, 1, 1, 3.0)
        gbLayout.setConstraints(lInstructedSpeed, gbc)
        this.add(lInstructedSpeed)

        gbc.update(4, 0, 2, 2, 7.3)
        gbLayout.setConstraints(l9, gbc)
        this.add(l9)

        gbc.update(4, 2, 1, 1, 1.3)
        val blanc43 = JLabel("", SwingConstants.CENTER)
        blanc13.preferredSize = Dimension(32, 20)
        gbLayout.setConstraints(blanc43, gbc)
        this.add(blanc43)

        gbc.update(4, 3, 1, 1, 1.3)
        gbLayout.setConstraints(l10, gbc)
        this.add(l10)

        gbc.update(5, 2, 1, 2, 6.0)
        gbLayout.setConstraints(l11, gbc)
        this.add(l11)

        gbc.update(6, 0, 1, 2, 2.0)
        gbLayout.setConstraints(l12, gbc)
        this.add(l12)


        gbc.update(7, 0, 1, 1, 1.0)
        gbLayout.setConstraints(bu, gbc)
        this.add(bu)

        gbc.update(7, 1, 1, 1, 1.0)
        gbLayout.setConstraints(bd, gbc)
        this.add(bd)

        gbc.update(7, 2, 1, 1, 1.0)
        gbLayout.setConstraints(bInfo, gbc)
        this.add(bInfo)

        gbc.update(7, 3, 1, 1, 1.0)
        gbLayout.setConstraints(bDelete, gbc)
        this.add(bDelete)

        //ATCType変更時にカラーリングを変更できるように登録
        typeJComponentMap[StripDataType.AtcType] = Pair(null, object : InfoUpdate {
            override fun update() {
                updateColorTheme()
            }
        })
    }

    /**
     * ATCTypeによるカラーテーマの取得
     */
    private fun getColorTheme(): Pair<Color, Color> {
        return when (stripData.radarData!!.getATCType(stripListGui.tile.airportICAO)) {
            ATCType.NotUnderMyControl -> Pair(Color(180, 180, 180), Color(100, 100, 100))
            ATCType.UnderMyControl -> Pair(Color(50, 50, 50), Color(220, 220, 220))
            ATCType.Departure -> Pair(Color(1, 207, 255), Color(231, 255, 255))
            ATCType.Arrival -> Pair(Color(255, 207, 0), Color(255, 255, 231))
        }
    }

    /**
     * ATCTypeによるカラーテーマを適用
     */
    fun updateColorTheme() {
        val (borderColor, backGroundColor) = getColorTheme()
        this.border = LineBorder(borderColor, 2)
        this.background = backGroundColor

        for (component in this.haveBorder) {
            component.border = LineBorder(borderColor, 2)
        }
        //callSignJLebel.background = boderColor
    }

    fun flash() {
        for (type in stripData.updatedList) {
            typeJComponentMap[type]?.run {
                second.update()
                first?.caretPosition = first?.document?.length ?: 0
            }
        }
        stripData.updatedList.clear()
    }

    //region 変更のイベントリスナー
    ///////////////////////////////////
    //  変更のイベントリスナー           //
    // Enterキーを押されたときに呼ばれる  //
    ///////////////////////////////////
    private fun updateCallSign(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.callSign = textField.text
        PacketHandlerAPM.sendPacketServer(StripDataWriteSync.syncCallSign(stripData.entityID, stripData.callSign))
    }

    private fun updateArrivalAirport(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.arrivalAirport = textField.text
        PacketHandlerAPM.sendPacketServer(StripDataWriteSync.syncArrival(stripData.entityID, stripData.arrivalAirport))
    }

    private fun updateInstructedAlt(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.instructedAltitude = textField.text.toInt()
        PacketHandlerAPM.sendPacketServer(
            StripDataWriteSync.syncInstructedAltitude(
                stripData.entityID,
                stripData.instructedAltitude
            )
        )
    }

    private fun updateTempAlt(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.tempAltitude = textField.text.toInt()
        PacketHandlerAPM.sendPacketServer(
            StripDataWriteSync.syncTempAltitude(
                stripData.entityID,
                stripData.tempAltitude
            )
        )
    }

    private fun updateInstructedHeading(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.instructedHeading = textField.text
        PacketHandlerAPM.sendPacketServer(
            StripDataWriteSync.syncInstructedHeading(
                stripData.entityID,
                stripData.instructedHeading
            )
        )
    }

    private fun updateInstructedSpeed(e: ActionEvent) {
        val textField = (e.source as JTextField)
        stripData.instructedSpeed = textField.text.toInt()
        PacketHandlerAPM.sendPacketServer(
            StripDataWriteSync.syncInstructedSpeed(
                stripData.entityID,
                stripData.instructedSpeed
            )
        )
    }

    private fun deleteStrip() {
        PacketHandlerAPM.sendPacketServer(
            ArtsDisplayDataSync.syncDelStrip(
                this.stripListGui.tile,
                this.stripData.entityID
            )
        )
    }
    //endregion

    //region 便利用ショートカット
    ///////////////////////
    //  便利用ショートカット //
    ///////////////////////

    interface InfoUpdate {
        fun update()
    }

    private fun JLabel(text: String?, horizontalAlignment: Int, dimension: Dimension): JLabel {
        return JLabel(text, horizontalAlignment).also { preferredSize = dimension }
    }

    private fun JTextFieldWithMapSet(
        stripDataType: StripDataType,
        text: String,
        hAlignment: Int,
        dimension: Dimension,
        intOnly: Boolean = false,
        limit: Int = 0
    ): JTextField {
        val ret = JTextField(stripData.getStringInType(stripDataType)).also { preferredSize = dimension }
        ret.horizontalAlignment = hAlignment
        ret.text = text

        typeJComponentMap[stripDataType] = Pair(ret, object : InfoUpdate {
            override fun update() {
                ret.text = stripData.getStringInType(stripDataType)
            }
        })
        if (intOnly || limit > 0) {
            (ret.document as AbstractDocument).documentFilter = IntegerLimitDocumentFilter(intOnly, limit)
        }
        return ret
    }

    //endregion
}
