package airportlight.radar.striplistgui

import airportlight.modcore.PacketHandlerAPM
import airportlight.radar.StripData
import airportlight.radar.StripDataType
import airportlight.radar.artsdisplay.ArtsDisplayTile
import airportlight.radar.system.RadarSystemClient
import airportlight.radar.system.syncmessage.StripDataWriteSync
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.relauncher.Side
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiScreen
import java.awt.Dimension
import java.awt.GraphicsEnvironment
import java.util.*
import javax.swing.BoxLayout
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
import kotlin.collections.HashMap
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set
import kotlin.math.max
import kotlin.math.min


class StripListGui(
    private val side: Side,
    val tile: ArtsDisplayTile,
    val minecraft: Minecraft,
    private val parentGui: GuiScreen
) :
    Thread() {
    private val canUse: Boolean = side == Side.CLIENT && !GraphicsEnvironment.isHeadless()
    private lateinit var frame: JFrame
    private var finished = false
    private val stripPanels = HashMap<Int, StripPanel>()
    private val scrollBody = JPanel()
    private var stripCnt = 0

    init {
        if (canUse) {
            //画面設定
            frame = JFrame("AirPortMod-Strip")
            frame.defaultCloseOperation = DO_NOTHING_ON_CLOSE
            frame.setSize(600, 140)
            frame.isLocationByPlatform = true
            //mainFrame.isResizable = false

            //スクロール用の下地を準備
            scrollBody.layout = BoxLayout(scrollBody, BoxLayout.Y_AXIS)
            //スクロール画面を用意
            val scroller = JScrollPane(scrollBody)

            //ストリップの見方ガイドを追加
            scrollBody.add(StripPanelGuide())

            //ストリップを登録
            for ((entityID, stripData) in RadarSystemClient.stripList) {
                val stripPanel = StripPanel(this, stripData)
                stripPanels[entityID] = stripPanel
                scrollBody.add(stripPanel)
            }

            stripCnt = scrollBody.componentCount
            frame.setSize(600, min(scrollBody.componentCount, 3) * 140 + 10)
            scrollBody.preferredSize = Dimension(500, (scrollBody.componentCount) * 100)

            //画面にスクロール画面を設定
            frame.contentPane.add(scroller)
            frame.isVisible = true
        }
    }

    /**
     * StripDataの件数などをもとに画面の大きさを再計算
     */
    private fun formReSize() {
        val max = if (scrollBody.componentCount < stripCnt) {
            //表示している件数が減っているならへった分小さく
            frame.size.height - 140 * (stripCnt - scrollBody.componentCount)
        } else {
            frame.size.height
        }
        //表示件数の増減を考慮した現在の画面のサイズを考慮しつつ、3件表示できるサイズまでは自動拡張
        frame.setSize(frame.size.width, max(min(scrollBody.componentCount, 3) * 140 + 10, max))
        scrollBody.preferredSize = Dimension(500, (scrollBody.componentCount) * 100)
        frame.revalidate()
        stripCnt = scrollBody.componentCount
    }

    override fun run() {
        if (canUse) {
            frame.isVisible = true
        }
        try {
            runThread()
        } catch (var6: Throwable) {
            finish()
            var6.printStackTrace()
        } finally {
            if (canUse) {
                frame.dispose()
            }
        }
    }

    @Throws(InterruptedException::class)
    private fun runThread() {
        if (side == Side.CLIENT) {
            while (!finished && minecraft.currentScreen == parentGui) {
                //Stripに更新があれば表示に反映
                if (updatedStripDataSet.isNotEmpty()) {
                    for (entityID in updatedStripDataSet) {
                        stripPanels[entityID]?.flash()
                    }
                    updatedStripDataSet.clear()
                }
                val sleepTime = 1000L

                //更新送信まちリストを処理
                if (sendReservationList.isNotEmpty()) {
                    val iterator = sendReservationList.iterator()
                    while (iterator.hasNext()) {
                        val (_, que) = iterator.next()
                        val newWaitCnt: Int = (que.cnt + sleepTime).toInt()
                        que.cnt = newWaitCnt

                        //最後の更新から規定時間立っているならサーバーに送信
                        if (sendWaitTime <= newWaitCnt) {
                            PacketHandlerAPM.sendPacketServer(que.message)
                            iterator.remove()
                        }
                    }
                }

                sleep(sleepTime)
            }
        }
    }

    /**
     * StripListGuiを終了
     */
    fun finish() {
        finished = true
    }

    /**
     * 更新のあったStripDataのEntityIDリスト
     */
    private var updatedStripDataSet: HashSet<Int> = HashSet<Int>()

    /**
     * 更新のあったStripDataの表示更新を要求
     */
    fun syncByEntityIDs(set: Set<Int>) {
        updatedStripDataSet.addAll(set)
    }

    /**
     * ストリップの表示追加
     * @param list Map< entityID, StripData >
     */
    fun addStrip(list: Map<Int, StripData>) {
        for ((entityID, stripData) in list) {
            val stripPanel = StripPanel(this, stripData)
            stripPanels[entityID] = stripPanel
            scrollBody.add(stripPanel)
        }

        formReSize()
    }

    /**
     * ストリップの削除
     * @param list List< entityID >
     */
    fun delStrip(list: List<Int>) {
        for (entityID in list) {
            stripPanels[entityID]?.let {
                scrollBody.remove(it)
            }
            stripPanels.remove(entityID)
        }
        formReSize()
    }

    /**
     * テキストエリアの入力終了→送信待ちリスト
     */
    private val sendReservationList = HashMap<DataID, Que>()

    /**
     * 更新の終了待ち時間 この時間を超えても更新がない場合更新終了とみなしてサーバーへ送信する
     */
    private val sendWaitTime = 2000

    /**
     * テキストエリアなどの内容更新をサーバーへ送信するよう要求
     *
     */
    fun requestSend(message: StripDataWriteSync) {
        sendReservationList[DataID(message.entityID, message.dataType)] = Que(0, message)
    }

    /**
     * 内容の同定用
     */
    data class DataID(var entityID: Int, val dataType: StripDataType)

    /**
     * 待機時間と送信内容
     */
    data class Que(var cnt: Int, val message: IMessage)
}
