package airportlight.radar

import airportlight.radar.system.RadarSettingVar
import airportlight.util.Vec3D
import airportlight.util.readVec3D
import airportlight.util.writeVec3D
import net.minecraft.util.MathHelper
import java.io.DataInput
import java.io.DataOutput

/**
 * レーダーのログ・表示用の各種情報
 */
class RadarData(val entityID: Int, nowPos: Vec3D) {
    constructor(
        entityID: Int,
        posLog: Array<Vec3D?>,
        nowVec: Vec3D,
        nowSpeed: Int,
        lostCnt: Int,
        controllerName: String,
        atcType: ATCType
    ) : this(entityID, posLog[0]!!) {
        this.posLog = posLog
        this.nowVec = nowVec
        this.nowSpeed = nowSpeed
        this.lostCnt = lostCnt
        this.controllerName = controllerName
        this.atcType = atcType
    }

    var controllerName: String = ""

    //ATCでの管理状況
    private var atcType: ATCType = ATCType.NotUnderMyControl

    fun setATCType(controllerName: String, atcType: ATCType) {
        if (atcType == ATCType.NotUnderMyControl) {
            this.controllerName = ""
        } else {
            this.controllerName = controllerName
        }
        this.atcType = atcType
    }

    fun getATCType(controllerName: String): ATCType {
        return if (this.controllerName == controllerName) {
            this.atcType
        } else {
            ATCType.NotUnderMyControl
        }
    }


    //レーダーデータ
    val nowAltitude //現在の高度b5
        get() = nowPos.y

    val altitudeChange: String //高度変化状況の矢印
        get() {
            return if (posLog[2] != null) {
                val ySpeed = nowPos.y - posLog[2]!!.y
                when {
                    ySpeed > 20 -> {
                        "↑"
                    }
                    ySpeed < -20 -> {
                        "↓"
                    }
                    else -> {
                        "→"
                    }
                }
            } else {
                "→"
            }
        }
    var scheduledPassagePoints //通過予定ポイント s8
            : String? = null
    var scheduledTransitTime //通過予定時間 s8
            : String? = null

    var nowVec: Vec3D = Vec3D(0.0, 0.0, 0.0)
    var nowSpeed //現在の速度
            = 0

    var posLog = arrayOfNulls<Vec3D>(RadarSettingVar.logSize) // 10sごとの過去の座標　10～50ｓ
    val nowPos: Vec3D  //現在座標 b1
        get() = posLog[0]!!
    var expectedPosition //将来の予想位置　1m~10m b3
            : Vec3D? = null

    //管理用データ
    var lostCnt = 0
    var stripData: StripData? = null


    init {
        posLog[0] = nowPos
    }

    fun isLost(): Boolean {
        return lostCnt > 0
    }

    /**
     * 現在座標の更新
     */
    fun updatePosOn5sSer(nowPos: Vec3D) {
        System.arraycopy(posLog, 0, posLog, 1, RadarSettingVar.logSize - 1)
        posLog[0] = nowPos
        if (posLog[1] != null) {
            nowVec = posLog[0]!!.minus(posLog[1])
            nowSpeed = MathHelper.floor_double(nowVec.sqrt() / RadarSettingVar.updateRadarInterval * 20)
        }
    }

    /**
     * サーバーからのレーダーアップデートを反映
     */
    fun updateRadarDataSerToCli(radarData: RadarData) {
        this.posLog = radarData.posLog
        this.nowVec = radarData.nowVec
        this.nowSpeed = radarData.nowSpeed
        this.lostCnt = radarData.lostCnt
        this.controllerName = radarData.controllerName
        this.atcType = radarData.atcType
    }

    /**
     * RadarDataを書き出し
     */
    fun writeData(out: DataOutput) {
        out.writeInt(entityID)

        //レーダーデータ
        var posCnt = 0
        for (pos in posLog) {
            if (pos != null) {
                posCnt++
            }
        }
        out.writeInt(posCnt)
        for (i in 0 until posCnt) {
            out.writeVec3D(posLog[i]!!)
        }
        out.writeVec3D(nowVec)
        out.writeInt(nowSpeed)
        out.writeInt(lostCnt)
        out.writeUTF(controllerName)
        out.writeUTF(atcType.name)
    }


    companion object {
        /**
         * RadarDataを読み取り
         */
        fun readData(input: DataInput): RadarData {
            val entityID = input.readInt()

            //レーダーデータ
            val posLog = arrayOfNulls<Vec3D>(RadarSettingVar.logSize)
            val posCnt = input.readInt()
            for (i in 0 until posCnt) {
                posLog[i] = input.readVec3D()
            }
            val nowVec = input.readVec3D()
            val nowSpeed = input.readInt()
            val lostCnt = input.readInt()
            val controllerName = input.readUTF()
            val atcType = ATCType.valueOf(input.readUTF())

            return RadarData(entityID, posLog, nowVec, nowSpeed, lostCnt, controllerName, atcType)
        }
    }
}
