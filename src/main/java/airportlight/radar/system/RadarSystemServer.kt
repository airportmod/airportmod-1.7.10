package airportlight.radar.system

import airportlight.modcore.PacketHandlerAPM
import airportlight.modcore.config.AirportConfig
import airportlight.radar.RadarData
import airportlight.radar.StripData
import airportlight.radar.system.syncmessage.RadarDataUpdateSerToCli
import airportlight.radar.system.syncmessage.StripDataMakeDeleteSync
import airportlight.util.ParentEntityGetter
import airportlight.util.Vec3D
import cpw.mods.fml.common.network.simpleimpl.IMessage
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityList
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.item.EntityXPOrb
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.server.MinecraftServer
import net.minecraft.util.MathHelper
import net.minecraft.world.World

object RadarSystemServer {
    //region 情報保持変数
    /**
     * スプリットデータリスト
     */
    @kotlin.jvm.JvmField
    var stripList = HashMap<Int, StripData>()

    /**
     * レーダーデータリスト
     */
    @kotlin.jvm.JvmField
    val radarDataList = HashMap<Int, RadarData>()

    /**
     * 出現したEntityの種類リスト
     */
    @kotlin.jvm.JvmField
    val typeNameList: HashSet<String> = HashSet<String>()

    //endregion

    //region 情報受信者リスト関連
    /**
     * クライアント受信者リスト
     */
    private var receiverPlayer = ArrayList<EntityPlayerMP>()

    /**
     * レーダー画面を開いてレーダー・ストリップの更新を受信開始
     */
    @JvmStatic
    fun addReceiverPlayer(player: EntityPlayerMP) {
        receiverPlayer.add(player)
    }


    /**
     * レーダー画面を閉じてレーダー・ストリップの更新受診を終了
     */
    @JvmStatic
    fun removeReceiverPlayer(player: EntityPlayerMP) {
        receiverPlayer.remove(player)
    }
    //endregion


    /**
     * レーダーの更新
     */
    @JvmStatic
    fun serverTick5s() {
        if (AirportConfig.noneLookType != null) {
            val addData = ArrayList<RadarData>()
            val updateData = ArrayList<RadarData>()
            val delData = ArrayList<Int>()

            //ロストカウント処理
            for (stripData in radarDataList.values) {
                if (stripData.lostCnt < RadarSettingVar.lostCntMax) {
                    stripData.lostCnt++
                }
            }

            val world: World = MinecraftServer.getServer().worldServers[0]
            val loadedEntityList = world.loadedEntityList as List<Entity>

            //全Entityについて精査
            for (i in loadedEntityList.indices) {
                //安全確認
                if (i >= loadedEntityList.size) {
                    break
                }

                //Entity取得
                val entity = loadedEntityList[i]

                //読み込み対象外を除外
                if (entity is EntityPlayer) continue
                if (entity is EntityItem) continue
                if (entity is EntityXPOrb) continue
                //Entityの親Entityを取得
                val parentEntity = ParentEntityGetter.getParent(entity)
                val entityTypeName = EntityList.getEntityString(parentEntity)
                //出現したEntityの種類リストを保存
                if (!typeNameList.contains(entityTypeName)) {
                    typeNameList.add(entityTypeName)
                }
                //読み込み対象外を除外
                if (!AirportConfig.noneLookType!!.contains(entityTypeName)) {
                    //すでに他のEntityの親として更新済みならスキップ
                    if (radarDataList[parentEntity.entityId]?.lostCnt == 0) {
                        continue
                    }
                    //建屋・地下のEntityは除外
                    if (
//                    if (world.blockExists(
//                            parentEntity.chunkCoordX,
//                            10,
//                            parentEntity.chunkCoordZ
//                        ) &&
                        world.canBlockSeeTheSky(
                            MathHelper.floor_double(parentEntity.posX),
                            MathHelper.floor_double(parentEntity.posY),
                            MathHelper.floor_double(parentEntity.posZ)
                        )
                    ) {
                        val entityID = parentEntity.entityId
                        if (radarDataList.containsKey(entityID)) {
                            //レーダーで捕捉済みなら更新
                            val data = radarDataList[entityID]!!
                            data.updatePosOn5sSer(Vec3D(parentEntity))
                            updateData.add(data)
                            data.lostCnt = 0
                        } else {
                            //新規登場なら追加
                            val data = RadarData(entityID, Vec3D(parentEntity))
                            radarDataList[entityID] = data
                            addData.add(data)
                            data.lostCnt = 0
                        }
                    }
                }
            }

            //ロストしているならレーダーから削除
            for (stripData in radarDataList.values) {
                if (stripData.isLost()) {
                    delData.add(stripData.entityID)
                }
            }
            for (id in delData) {
                radarDataList.remove(id)
            }

            //DataSendToClient
            sendMessageToRadarReceivers(RadarDataUpdateSerToCli(addData, updateData, delData))
        }
    }

    /**
     * レーダー関係の情報送信用
     */
    fun sendMessageToRadarReceivers(message: IMessage) {
        PacketHandlerAPM.sendPacketPlayers(message, receiverPlayer)
    }

    fun getStripData(entityID: Int): StripData? {
        return stripList[entityID]
    }

    fun getRadarData(entityID: Int): RadarData? {
        return radarDataList[entityID]
    }

    /**
     * レーダーデータを選択してストリップを追加
     */
    fun makeAndAddStripData(entityID: Int, radarData: RadarData, controllerName: String) {
        if (!stripList.containsKey(entityID)) {
            //すでにStripData作成済みでなければ作成・登録
            val strip = StripData(radarData, controllerName)
            stripList[entityID] = strip
            radarData.stripData = strip
            sendMessageToRadarReceivers(StripDataMakeDeleteSync(arrayListOf(strip)))
        }
    }

    /**
     * StripDataの削除
     */
    fun delStripData(stripData: StripData) {
        if (stripList.containsKey(stripData.entityID)) {
            stripData.radarData?.stripData = null
            stripList.remove(stripData.entityID)
        }
    }

    /**
     * レーダー、ストリップのリセット
     */
    fun resetRadarStripData() {
        radarDataList.clear()
        stripList.clear()
    }
}
