package airportlight.radar.system

/**
 * レーダー周りの設定保存用
 */
object RadarSettingVar {
    /**
     * Entityがいた座標をどの回数まで保存しておくか
     */
    const val logSize = 11

    /**
     * レーダー情報の更新間隔
     */
    const val updateRadarInterval = 100

    /**
     * レーダーロストの回数を数える最大値
     */
    const val lostCntMax = 10
}