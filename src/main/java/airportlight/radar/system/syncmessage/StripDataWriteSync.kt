package airportlight.radar.system.syncmessage

import airportlight.radar.ATCType
import airportlight.radar.StripData
import airportlight.radar.StripDataType
import airportlight.radar.artsdisplay.ArtsDisplayGui
import airportlight.radar.system.RadarSystemClient
import airportlight.radar.system.RadarSystemServer
import airportlight.util.guiScreen
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf

class StripDataWriteSync : IMessage, IMessageHandler<StripDataWriteSync, IMessage> {
    var entityID: Int = 0
    lateinit var dataType: StripDataType
    private lateinit var strObj: String

    constructor()
    private constructor(entityID: Int, dataType: StripDataType, strObj: String) {
        this.entityID = entityID
        this.dataType = dataType
        this.strObj = strObj
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.entityID)
        buf.writeInt(this.dataType.id)
        ByteBufUtils.writeUTF8String(buf, this.strObj)
    }

    override fun fromBytes(buf: ByteBuf) {
        this.entityID = buf.readInt()
        this.dataType = StripDataType.getType(buf.readInt())
        this.strObj = ByteBufUtils.readUTF8String(buf)
    }

    override fun onMessage(message: StripDataWriteSync, ctx: MessageContext): IMessage? {
        val stripData = getStrip(message.entityID, ctx)
        if (stripData != null) {
            typeFunc[message.dataType]?.onTypeMessage(stripData, message.strObj)
            if (ctx.side.isServer) {
                RadarSystemServer.sendMessageToRadarReceivers(message)
            } else {
                stripData.updatedList.add(message.dataType)
                (ctx.guiScreen as? ArtsDisplayGui)?.stripListGui?.syncByEntityIDs(setOf(message.entityID))
            }
        }
        return null
    }

    //TypeFunc に受け取り時の挙動をを実装するためのインターフェース
    interface ITypeFunc {
        fun onTypeMessage(stripData: StripData, strObj: String)
    }

    companion object {
        fun getStrip(entityID: Int, ctx: MessageContext): StripData? {
            return if (ctx.side.isClient) {
                RadarSystemClient.getStripData(entityID)
            } else {
                RadarSystemServer.getStripData(entityID)
            }
        }

        //region Message受取時の各DataTypeごとの挙動
        val typeFunc = mapOf<StripDataType, ITypeFunc>(
            Pair(StripDataType.CallSign, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.callSign = strObj
                }
            }),

            Pair(StripDataType.ArrivalAirport, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.arrivalAirport = strObj
                }
            }),

            Pair(StripDataType.Route, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.route = strObj
                }
            }),

            Pair(StripDataType.InstructedAltitude, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.instructedAltitude = strObj.toInt()
                }
            }),

            Pair(StripDataType.TempAltitude, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.tempAltitude = strObj.toInt()
                }
            }),

            Pair(StripDataType.InstructedHeading, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.instructedHeading = strObj
                }
            }),

            Pair(StripDataType.InstructedSpeed, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.instructedSpeed = strObj.toInt()
                }
            }),

            Pair(StripDataType.MemoLong, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.memoLong = strObj
                }
            }),

            Pair(StripDataType.AtcType, object : ITypeFunc {
                override fun onTypeMessage(
                    stripData: StripData,
                    strObj: String
                ) {
                    stripData.atcType = ATCType.valueOf(strObj)
                }
            }),
        )
        //endregion


        //region DataType別メッセージコンストラクタ

        fun syncCallSign(entityID: Int, callSign: String): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.CallSign, callSign)
        }

        fun syncArrival(entityID: Int, arrivalAirport: String): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.ArrivalAirport, arrivalAirport)
        }

        fun syncRoute(entityID: Int, route: String): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.Route, route)
        }

        fun syncInstructedAltitude(entityID: Int, alt: Int): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.InstructedAltitude, alt.toString())
        }

        fun syncTempAltitude(entityID: Int, alt: Int): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.TempAltitude, alt.toString())
        }

        fun syncInstructedHeading(entityID: Int, heading: String): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.InstructedHeading, heading)
        }

        fun syncInstructedSpeed(entityID: Int, speed: Int): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.InstructedSpeed, speed.toString())
        }

        fun syncMemoLong(entityID: Int, memoLong: String): StripDataWriteSync {
            return StripDataWriteSync(entityID, StripDataType.MemoLong, memoLong)
        }

        //endregion
    }

}

