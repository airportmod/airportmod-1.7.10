package airportlight.radar.system.syncmessage

import airportlight.radar.RadarData
import airportlight.radar.StripData
import airportlight.radar.system.RadarSystemClient
import airportlight.util.*
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import java.util.*

/**
 * レーダー画面を開いたプレイヤーへ最新のレーダー・ストリップ情報を一括でサーバーから送信
 * (c) 2020 anatawa12
 * edit kuma
 */
class RadarNewStripDataSerToCli : IMessage, IMessageHandler<RadarNewStripDataSerToCli, IMessage> {
    private lateinit var newRadar: List<RadarData>
    private lateinit var newStrip: List<StripData>

    @Deprecated("", level = DeprecationLevel.HIDDEN)
    @Suppress("unused")
    constructor()

    constructor(
        newRadar: HashMap<Int, RadarData>,
        newStrip: HashMap<Int, StripData>
    ) {
        this.newRadar = newRadar.values.toList()
        this.newStrip = newStrip.values.toList()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.outputStream()
            .gzip()
            .forData()
            .use { dos ->
                dos.writeList(this.newRadar) { it.writeData(dos) }
                dos.writeList(this.newStrip) { it.writeData(dos) }
            }
    }

    override fun fromBytes(buf: ByteBuf) {
        buf.readBytes(buf.readableBytes()).array()
            .inputStream()
            .gunzip()
            .toData()
            .use { dis ->
                this.newRadar = dis.readList { RadarData.readData(dis) }
                this.newStrip = dis.readList { StripData.readData(dis) }
            }
    }


    override fun onMessage(message: RadarNewStripDataSerToCli, ctx: MessageContext): IMessage? {
        RadarSystemClient.newDataSerToCli(message.newRadar, message.newStrip)
        return null
    }
}