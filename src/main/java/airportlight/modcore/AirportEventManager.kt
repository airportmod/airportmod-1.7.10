package airportlight.modcore

import airportlight.ModAirPortLight
import airportlight.radar.system.RadarSettingVar
import airportlight.radar.system.RadarSystemServer.serverTick5s
import airportlight.util.Vec3I
import cpw.mods.fml.common.eventhandler.SubscribeEvent
import cpw.mods.fml.common.gameevent.TickEvent
import cpw.mods.fml.common.gameevent.TickEvent.RenderTickEvent
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent
import net.minecraft.world.World
import java.util.*

object AirportEventManager {
    @SubscribeEvent
    fun onRenderTickEvent(event: RenderTickEvent) {
        if (event.phase == TickEvent.Phase.END) {
            ModAirPortLight.proxy.onRenderTickPost()
        }
    }

    var cnt5s = 0
    var removeLights = HashMap<World, LinkedList<Pair<Vec3I, LinkedHashSet<Vec3I>>>>()

    @SubscribeEvent
    fun onServerTickEvent(event: ServerTickEvent) {
        if (event.phase == TickEvent.Phase.START) {
            cnt5s++
            if (cnt5s >= RadarSettingVar.updateRadarInterval) {
                serverTick5s()
                cnt5s = 0
            }

            if (removeLights.isNotEmpty()) {
                var cnt = 0
                val iteratorCurrentMaps = removeLights.iterator()
                for ((world, lists) in iteratorCurrentMaps) {
                    val iteratorPos = lists.iterator()
                    End@
                    for ((_, list) in iteratorPos) {
                        val iteratorList = list.iterator()
                        for (xyz in iteratorList) {
                            if (world.getBlock(xyz.x, xyz.y, xyz.z) == InstanceList.blockLightAir) {
                                world.setBlockToAir(xyz.x, xyz.y, xyz.z)
                            }
                            iteratorList.remove()
                            cnt++
                        }
                        iteratorPos.remove()
                    }
                    iteratorCurrentMaps.remove()
                }
            }
        }
    }
}
