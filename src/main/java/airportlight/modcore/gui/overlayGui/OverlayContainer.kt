package airportlight.modcore.gui.overlayGui

import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer

open class OverlayContainer {
    open fun canInteractWith(): Boolean {
        return true
    }

    class OverlayContainerRaidEntity(val player: EntityPlayer, val riddenEntity: Entity) : OverlayContainer() {
        override fun canInteractWith(): Boolean {
            return riddenEntity.riddenByEntity == player
        }
    }
}
