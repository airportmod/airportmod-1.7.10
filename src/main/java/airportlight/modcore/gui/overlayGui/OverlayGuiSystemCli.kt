package airportlight.modcore.gui.overlayGui

import airportlight.towingcar.TowingCarDriverGui
import cpw.mods.fml.common.eventhandler.SubscribeEvent
import cpw.mods.fml.common.gameevent.InputEvent
import cpw.mods.fml.common.gameevent.TickEvent
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.ScaledResolution
import net.minecraft.entity.player.EntityPlayer
import net.minecraftforge.client.event.RenderGameOverlayEvent
import net.minecraftforge.event.world.WorldEvent
import org.lwjgl.opengl.GL11

@SideOnly(Side.CLIENT)
class OverlayGuiSystemCli : OverlayGuiSystem() {
    val minecraft: Minecraft by lazy { Minecraft.getMinecraft() }

    override fun openTowingCarDriverGui(player: EntityPlayer) {
        this.overlayScreen = TowingCarDriverGui(player)
    }


    @SubscribeEvent
    fun onKeyPress(e: InputEvent.KeyInputEvent) {
        keyPress()
    }

    @SubscribeEvent
    fun onRenderScreen(e: RenderGameOverlayEvent.Post) {
        if (e.type == RenderGameOverlayEvent.ElementType.ALL && this.innerOverlayScreen != null) {
            GL11.glPushMatrix()

//            GL11.glAlphaFunc(GL11.GL_GREATER, 0.1f)
            this.innerOverlayScreen!!.drawScreen(0, 0, 0f)
            GL11.glPopMatrix()
            GL11.glEnable(GL11.GL_BLEND)
        }
    }

    @SubscribeEvent
    fun onClientTick(e: TickEvent.ClientTickEvent) {
        if (this.innerOverlayScreen != null) {
            if (!this.innerOverlayScreen!!.overlayContainer.canInteractWith()) {
                this.innerOverlayScreen!!.closeGui()
                this.overlayScreen = null
                this.innerOverlayScreen = null
                return
            }
            this.innerOverlayScreen!!.updateScreen()
            keyPress()
        }
    }

    @SubscribeEvent
    fun onWorldUnload(e: WorldEvent.Unload) {
        this.overlayScreen = null
        this.innerOverlayScreen = null
    }

    override var overlayScreen: OverlayScreenCommon? = null
        set(value) {
            field = value
            if (value is OverlayScreen) {
                innerOverlayScreen = value
                val scaledResolution = ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight)
                val i = scaledResolution.scaledWidth
                val j = scaledResolution.scaledHeight
                innerOverlayScreen!!.setWorldAndResolution(this.minecraft, i, j)
            }
        }
        get() = innerOverlayScreen

    private var innerOverlayScreen: OverlayScreen? = null


    fun keyPress() {
        innerOverlayScreen?.handleKeyboardInput()
    }
}
