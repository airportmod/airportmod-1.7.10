package airportlight.modcore.gui.overlayGui

import net.minecraft.entity.player.EntityPlayer

open class OverlayGuiSystem {
    open var overlayScreen: OverlayScreenCommon? = null
    open fun openTowingCarDriverGui(player: EntityPlayer) {}

    fun closeGui() {
        overlayScreen?.onGuiClosed()
        overlayScreen = null
    }
}
