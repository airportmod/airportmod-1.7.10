package airportlight.modcore.gui.overlayGui

import airportlight.ModAirPortLight

abstract class OverlayScreenCommon {
    fun closeGui() {
        ModAirPortLight.overlayGuiSystem.closeGui()
    }

    open fun onGuiClosed() {}
}
