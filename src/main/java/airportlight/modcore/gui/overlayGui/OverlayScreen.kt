package airportlight.modcore.gui.overlayGui

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.Gui
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.client.renderer.Tessellator
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11

@SideOnly(Side.CLIENT)
abstract class OverlayScreen() : OverlayScreenCommon() {
    lateinit var mc: Minecraft
    lateinit var fontRendererObj: FontRenderer
    var overlayContainer = OverlayContainer()
    var width: Int = 0
    var height: Int = 0
    var allowUserInput: Boolean = false

    /** The X size of the inventory window in pixels.  */
    protected var xSize = 176

    /** The Y size of the inventory window in pixels.  */
    protected var ySize = 166

    /** Starting X position for the Gui. Inconsistent use for Gui backgrounds.  */
    protected var guiLeft = 0

    /** Starting Y position for the Gui. Inconsistent use for Gui backgrounds.  */
    protected var guiTop = 0

    protected var zLevel: Float = 0f


    constructor(player: EntityPlayer, riddenEntity: Entity) : this() {
        overlayContainer = OverlayContainer.OverlayContainerRaidEntity(player, riddenEntity)
    }


    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    open fun initGui() {
        this.guiLeft = (width - this.xSize) / 2
        this.guiTop = (height - this.ySize) / 2
    }

    /**
     * Draws the screen and all the components in it.
     */
    fun drawScreen(p_73863_1_: Int, p_73863_2_: Int, p_73863_3_: Float) {
        drawDefaultBackground()
        this.drawGuiContainerBackgroundLayer(p_73863_3_, p_73863_1_, p_73863_2_)
    }

    open fun drawDefaultBackground() {
        this.drawWorldBackground(0)
    }

    open fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            this.drawGradientRect(0, 0, this.width, this.height, -1072689136, -804253680)
        } else {
            this.drawBackground(p_146270_1_)
        }
    }

    fun drawBackground(p_146278_1_: Int) {
        GL11.glDisable(2896)
        GL11.glDisable(2912)
        val tessellator = Tessellator.instance
        mc.textureManager.bindTexture(Gui.optionsBackground)
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        val f = 32.0f
        tessellator.startDrawingQuads()
        tessellator.setColorOpaque_I(4210752)
        tessellator.addVertexWithUV(0.0, height.toDouble(), 0.0, 0.0, (height.toFloat() / f + p_146278_1_.toFloat()).toDouble())
        tessellator.addVertexWithUV(
            width.toDouble(),
            height.toDouble(),
            0.0,
            (width.toFloat() / f).toDouble(),
            (height.toFloat() / f + p_146278_1_ as Float).toDouble()
        )
        tessellator.addVertexWithUV(width.toDouble(), 0.0, 0.0, (width.toFloat() / f).toDouble(), p_146278_1_.toDouble())
        tessellator.addVertexWithUV(0.0, 0.0, 0.0, 0.0, p_146278_1_.toDouble())
        tessellator.draw()
    }

    protected abstract fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, p_146976_2_: Int, p_146976_3_: Int)

    open fun updateScreen() {
        if (!mc.thePlayer.isEntityAlive || mc.thePlayer.isDead) {
            onGuiClosed()
        }
    }

    fun setWorldAndResolution(p_146280_1_: Minecraft, p_146280_2_: Int, p_146280_3_: Int) {
        this.mc = p_146280_1_
        this.fontRendererObj = p_146280_1_.fontRenderer
        this.width = p_146280_2_
        this.height = p_146280_3_
        this.initGui()
    }

    open fun handleKeyboardInput() {
        if (Keyboard.getEventKeyState()) {
            this.keyTyped(Keyboard.getEventCharacter(), Keyboard.getEventKey())
        }

        mc.func_152348_aa()
    }

    protected open fun keyTyped(p_73869_1_: Char, p_73869_2_: Int) {}

    open fun doesGuiPauseGame(): Boolean {
        return true
    }

    protected open fun drawGradientRect(
        p_drawGradientRect_1_: Int,
        p_drawGradientRect_2_: Int,
        p_drawGradientRect_3_: Int,
        p_drawGradientRect_4_: Int,
        p_drawGradientRect_5_: Int,
        p_drawGradientRect_6_: Int
    ) {
        val var7 = (p_drawGradientRect_5_ shr 24 and 255).toFloat() / 255.0f
        val var8 = (p_drawGradientRect_5_ shr 16 and 255).toFloat() / 255.0f
        val var9 = (p_drawGradientRect_5_ shr 8 and 255).toFloat() / 255.0f
        val var10 = (p_drawGradientRect_5_ and 255).toFloat() / 255.0f
        val var11 = (p_drawGradientRect_6_ shr 24 and 255).toFloat() / 255.0f
        val var12 = (p_drawGradientRect_6_ shr 16 and 255).toFloat() / 255.0f
        val var13 = (p_drawGradientRect_6_ shr 8 and 255).toFloat() / 255.0f
        val var14 = (p_drawGradientRect_6_ and 255).toFloat() / 255.0f
        GL11.glDisable(3553)
        GL11.glEnable(3042)
        GL11.glDisable(3008)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glShadeModel(7425)
        val var15 = Tessellator.instance
        var15.startDrawingQuads()
        var15.setColorRGBA_F(var8, var9, var10, var7)
        var15.addVertex(p_drawGradientRect_3_.toDouble(), p_drawGradientRect_2_.toDouble(), zLevel.toDouble())
        var15.addVertex(p_drawGradientRect_1_.toDouble(), p_drawGradientRect_2_.toDouble(), zLevel.toDouble())
        var15.setColorRGBA_F(var12, var13, var14, var11)
        var15.addVertex(p_drawGradientRect_1_.toDouble(), p_drawGradientRect_4_.toDouble(), zLevel.toDouble())
        var15.addVertex(p_drawGradientRect_3_.toDouble(), p_drawGradientRect_4_.toDouble(), zLevel.toDouble())
        var15.draw()
        GL11.glShadeModel(7424)
        GL11.glDisable(3042)
        GL11.glEnable(3008)
        GL11.glEnable(3553)
    }
}
