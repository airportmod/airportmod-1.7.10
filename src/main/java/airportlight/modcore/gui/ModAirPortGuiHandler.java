package airportlight.modcore.gui;

import airportlight.blocks.facility.localizer.LocalizerGui;
import airportlight.blocks.facility.localizer.LocalizerTile;
import airportlight.blocks.facility.pbb.PBBDriverGui;
import airportlight.blocks.facility.pbb.PBBDriverSeat;
import airportlight.blocks.facility.pbb.PBBSettingGui;
import airportlight.blocks.facility.pbb.TilePBB;
import airportlight.blocks.light.approachlight.ApproachLightGui;
import airportlight.blocks.light.approachlight.ApproachLightTile;
import airportlight.blocks.light.obstaclelight.ObstacleLightGui;
import airportlight.blocks.light.obstaclelight.TileObstacleLight;
import airportlight.blocks.light.papi.PAPIGui;
import airportlight.blocks.light.papi.TilePAPI;
import airportlight.blocks.light.runwaydistancemarkerlight.RunwayDistanceMarkerGui;
import airportlight.blocks.light.runwaydistancemarkerlight.TileRunwayDistanceMarkerLight;
import airportlight.blocks.markings.gloundsine.GroundSineGui;
import airportlight.blocks.markings.gloundsine.GroundSineTile;
import airportlight.blocks.markings.guidepanel.GuidePanelGui;
import airportlight.blocks.markings.guidepanel.TileGuidePanel;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsGui;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsTile;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsGui;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsTile;
import airportlight.blocks.markings.runwaynumber.RunwayNumberGui;
import airportlight.blocks.markings.runwaynumber.TileRunwayNumber;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsGui;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsTile;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsGui;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsTile;
import airportlight.landingpoint.LandingPointGui;
import airportlight.landingpoint.TileLandingPoint;
import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.GuiID;
import airportlight.modsystem.navigation.navsetting.MessageReqNavSetting;
import airportlight.modsystem.navigation.navsetting.NavSettingGui;
import airportlight.modsystem.navigation.vordme.TileVORDME;
import airportlight.modsystem.navigation.vordme.VORDMEGui;
import airportlight.radar.artsdisplay.ArtsDisplayGui;
import airportlight.radar.artsdisplay.ArtsDisplayTile;
import airportlight.towingcar.TowingCarDriverGui;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ModAirPortGuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (GuiID.getById(ID)) {
            case TowingCarDriver:
            case LandingPoint:
            case NavSetting:
            case VORDMESetting:
            case GuidePanel:
            case ApproachLight:
            case Localizer:
            case PBBDrive:
            case DistanceMarker:
            case GroundSine:
            case Papi:
            case PBB:
            case RunwayThresholdMarkings:
            case RunwayTouchdownZoneMarkings:
            case RunwayHoldPositionMarkings:
            case RunwayAimingPointMarkings:
            case ObstacleLight:
                return new ContainerAirPort();
            case RunwayNumber:
                TileEntity tile = world.getTileEntity(x, y, z);
                if (tile instanceof TileRunwayNumber) {
                    return new ContainerAirPort();
                }
                return null;
            case Arts:
                return new RadarContainerAirPortSer((EntityPlayerMP) player);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (GuiID.getById(ID)) {
            case TowingCarDriver:
                return new TowingCarDriverGui(player);
            case RunwayNumber:
                TileEntity tileRN = world.getTileEntity(x, y, z);
                if (tileRN instanceof TileRunwayNumber) {
                    return new RunwayNumberGui((TileRunwayNumber) tileRN);
                }
                return null;
            case LandingPoint:
                TileEntity tileLP = world.getTileEntity(x, y, z);
                if (tileLP instanceof TileLandingPoint) {
                    return new LandingPointGui((TileLandingPoint) tileLP);
                }
                return null;
            case NavSetting:
                PacketHandlerAPM.sendPacketServer(new MessageReqNavSetting());
                return new NavSettingGui();
            case VORDMESetting:
                TileEntity tileVORDME = world.getTileEntity(x, y, z);
                if (tileVORDME instanceof TileVORDME) {
                    return new VORDMEGui((TileVORDME) tileVORDME);
                }
                return null;
            case GuidePanel:
                TileEntity tileGuidePanel = world.getTileEntity(x, y, z);
                if (tileGuidePanel instanceof TileGuidePanel) {
                    return new GuidePanelGui((TileGuidePanel) tileGuidePanel);
                }
                return null;
            case ApproachLight:
                TileEntity tileAL = world.getTileEntity(x, y, z);
                if (tileAL instanceof ApproachLightTile) {
                    return new ApproachLightGui((ApproachLightTile) tileAL);
                }
                return null;
            case Localizer:
                TileEntity tileLo = world.getTileEntity(x, y, z);
                if (tileLo instanceof LocalizerTile) {
                    return new LocalizerGui((LocalizerTile) tileLo);
                }
                return null;
            case Arts:
                TileEntity tileAr = world.getTileEntity(x, y, z);
                if (tileAr instanceof ArtsDisplayTile) {
                    return new ArtsDisplayGui((ArtsDisplayTile) tileAr);
                }
                return null;
            case PBBDrive:
                if (player.ridingEntity instanceof PBBDriverSeat) {
                    return new PBBDriverGui(player);
                }
                return null;
            case DistanceMarker:
                TileEntity tileDML = world.getTileEntity(x, y, z);
                if (tileDML instanceof TileRunwayDistanceMarkerLight) {
                    return new RunwayDistanceMarkerGui((TileRunwayDistanceMarkerLight) tileDML);
                }
                return null;
            case GroundSine:
                TileEntity tileGroundSine = world.getTileEntity(x, y, z);
                if (tileGroundSine instanceof GroundSineTile) {
                    return new GroundSineGui((GroundSineTile) tileGroundSine);
                }
                return null;
            case Papi:
                TileEntity tilePAPI = world.getTileEntity(x, y, z);
                if (tilePAPI instanceof TilePAPI) {
                    return new PAPIGui((TilePAPI) tilePAPI);
                }
                return null;
            case PBB:
                TileEntity tilePBB = world.getTileEntity(x, y, z);
                if (tilePBB instanceof TilePBB) {
                    return new PBBSettingGui((TilePBB) tilePBB);
                }
                return null;
            case RunwayThresholdMarkings:
                TileEntity tileRunwayThreshold = world.getTileEntity(x, y, z);
                if (tileRunwayThreshold instanceof RunwayThresholdMarkingsTile) {
                    return new RunwayThresholdMarkingsGui((RunwayThresholdMarkingsTile) tileRunwayThreshold);
                }
                return null;
            case RunwayTouchdownZoneMarkings:
                TileEntity tileRunwayTouchdownZone = world.getTileEntity(x, y, z);
                if (tileRunwayTouchdownZone instanceof RunwayTouchdownZoneMarkingsTile) {
                    return new RunwayTouchdownZoneMarkingsGui((RunwayTouchdownZoneMarkingsTile) tileRunwayTouchdownZone);
                }
                return null;
            case RunwayHoldPositionMarkings:
                TileEntity tileRunwayHoldPositionMarkings = world.getTileEntity(x, y, z);
                if (tileRunwayHoldPositionMarkings instanceof RunwayHoldPositionMarkingsTile) {
                    return new RunwayHoldPositionMarkingsGui((RunwayHoldPositionMarkingsTile) tileRunwayHoldPositionMarkings);
                }
                return null;
            case RunwayAimingPointMarkings:
                TileEntity tileRunwayAimingPointMarkings = world.getTileEntity(x, y, z);
                if (tileRunwayAimingPointMarkings instanceof RunwayAimingPointMarkingsTile) {
                    return new RunwayAimingPointMarkingsGui((RunwayAimingPointMarkingsTile) tileRunwayAimingPointMarkings);
                }
                return null;
            case ObstacleLight:
                TileEntity tileObstacleLight = world.getTileEntity(x, y, z);
                if (tileObstacleLight instanceof TileObstacleLight) {
                    return new ObstacleLightGui((TileObstacleLight) tileObstacleLight);
                }
                return null;

        }
        return null;
    }
}
