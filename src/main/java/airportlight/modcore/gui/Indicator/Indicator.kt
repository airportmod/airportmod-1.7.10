package airportlight.modcore.gui.Indicator

import net.minecraft.client.gui.GuiButton
import kotlin.math.roundToInt

abstract class Indicator(val xPos: Double, val yPos: Double, val size: Double, var indicator: Boolean) :
    GuiButton(-1, xPos.roundToInt(), yPos.roundToInt(), size.roundToInt(), size.roundToInt(), "") {
    protected val scale: Double = size / 255
    private val scale1: Double

    init {
        scale1 = 1 / scale
        enabled = false
    }
}