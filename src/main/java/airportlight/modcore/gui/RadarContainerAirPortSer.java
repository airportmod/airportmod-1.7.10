package airportlight.modcore.gui;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.radar.system.RadarSystemServer;
import airportlight.radar.system.syncmessage.RadarNewStripDataSerToCli;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

public class RadarContainerAirPortSer extends ContainerAirPort {
    public RadarContainerAirPortSer(EntityPlayerMP player) {
        RadarSystemServer.addReceiverPlayer(player);
        PacketHandlerAPM.sendPacketEPM(new RadarNewStripDataSerToCli(RadarSystemServer.radarDataList, RadarSystemServer.stripList), player);
    }

    @Override
    public void onContainerClosed(EntityPlayer player) {
        if (player instanceof EntityPlayerMP) {
            RadarSystemServer.removeReceiverPlayer((EntityPlayerMP) player);
        }
        super.onContainerClosed(player);
    }
}
