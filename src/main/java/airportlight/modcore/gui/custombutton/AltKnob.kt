package airportlight.modcore.gui.custombutton

import airportlight.ModAirPortLight
import airportlight.util.Consumer
import net.minecraft.util.ResourceLocation

class AltKnob(id: Int, x: Int, y: Int, size: Int, onValueChange: Consumer) : SimpleKnob(id, x, y, size, onValueChange) {
    override val buttonTextures: ResourceLocation =
        ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/ap_alt_knob.png")
}