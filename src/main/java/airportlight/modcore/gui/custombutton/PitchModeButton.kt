package airportlight.modcore.gui.custombutton

import airportlight.modsystem.navigation.autopilot.EnumPitchMode
import airportlight.util.Pair
import net.minecraft.util.ResourceLocation
import java.util.*

class PitchModeButton(
    id: Int,
    x: Int,
    y: Int,
    size: Int,
    val mode: EnumPitchMode,
    lightMode: EnumButtonLightMode = EnumButtonLightMode.OFF
) : EnumModeButton(id, x, y, size, lightMode) {

    companion object {
        val btSwitch_ON = ResourceLocation("airportlight:textures/gui/ap_pitch_on.png")
        val btSwitch_StandBy = ResourceLocation("airportlight:textures/gui/ap_roll_standby.png")
        val btSwitch_OFF = ResourceLocation("airportlight:textures/gui/ap_pitch_off.png")

        val bottonTextureIndexMap =
            EnumMap<EnumPitchMode, Pair<Int, Int>>(
                EnumPitchMode::class.java
            )

        init {
            bottonTextureIndexMap[EnumPitchMode.VNAV] =
                Pair(0, 0)
            bottonTextureIndexMap[EnumPitchMode.VS] =
                Pair(1, 0)
            bottonTextureIndexMap[EnumPitchMode.ALTHOLD] =
                Pair(0, 1)
            bottonTextureIndexMap[EnumPitchMode.LVLCHG] =
                Pair(1, 1)
        }
    }

    override fun getTexturue(lightMode: EnumButtonLightMode): ResourceLocation {
        return when (this.lightMode) {
            EnumButtonLightMode.OFF -> btSwitch_OFF
            EnumButtonLightMode.StandBy -> btSwitch_StandBy
            EnumButtonLightMode.ON -> btSwitch_ON
        }
    }

    public override fun setEnabled(enabled: Boolean): PitchModeButton {
        this.enabled = enabled
        return this
    }

    override val txOffset: Int
    override val tyOffset: Int

    init {
        val offset = bottonTextureIndexMap[mode]
        txOffset = offset!!.key
        tyOffset = offset.value
    }
}