package airportlight.modcore.gui.custombutton

import airportlight.util.Consumer
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

abstract class SimpleKnob(id: Int, x: Int, y: Int, val size: Int, private val onValueChange: Consumer) :
    GuiButton(id, x, y, size, size, ""), IdoWheel {
    abstract val buttonTextures: ResourceLocation

    val offset: Double = 255.0 / 2.0
    var scale: Double = 0.0
    var scale1: Double = 0.0

    init {
        scale = size.toDouble() / 255
        scale1 = 1 / scale
    }

    protected var knobAngle = 0.0

    override fun drawButton(p_146112_1_: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        this.field_146123_n =
            (p_146112_2_ >= this.xPosition - size) && (p_146112_3_ >= this.yPosition - size) && (p_146112_2_ < this.xPosition + size) && (p_146112_3_ < this.yPosition + size)
        GL11.glEnable(GL11.GL_BLEND)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        GL11.glTranslated(this.xPosition.toDouble(), this.yPosition.toDouble(), 0.0)
        GL11.glScaled(scale, scale, scale)
        p_146112_1_.textureManager.bindTexture(buttonTextures)
        GL11.glRotated(knobAngle.toDouble(), 0.0, 0.0, 1.0)
        GL11.glTranslated(-offset, -offset, 0.0)
        this.drawTexturedModalRect(0, 0, 0, 0, 255, 255)
        if (!enabled) {
            drawGradientRect(0, 0, 255, 255, -0x5fefeff0, -0x5fafafb0)
        }
        GL11.glTranslated(offset, offset, 0.0)
        GL11.glRotated(-knobAngle.toDouble(), 0.0, 0.0, 1.0)
        GL11.glScaled(scale1, scale1, scale1)
        GL11.glPopMatrix()
        this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_)
    }

    override fun doWheel(mouseX: Int, mouseY: Int, dWheel: Int) {
        if (enabled) {
            val dx: Double = this.xPosition - mouseX.toDouble()
            val dy: Double = this.yPosition - mouseY.toDouble()
            val dxy2: Double = dx * dx + dy * dy
            val range: Double = this.size * this.size * 0.5 * 0.5
            if (dxy2 < range) {
                var dW: Int = dWheel
                if (dW != 0) {
                    dW /= 120
                    dW = executeDWheel(dW)
                    knobAngle += dW * 18
                    knobAngle %= 360
                    onValueChange.accept(dW)
                }
            }
        }
    }

    open fun executeDWheel(dWheel: Int): Int {
        return dWheel * dWheel * if (dWheel < 0) -1 else 1
    }

    override fun mousePressed(mc: Minecraft?, mouseX: Int, mouseY: Int): Boolean {
        val dx: Double = this.xPosition - mouseX.toDouble()
        val dy: Double = this.yPosition - mouseY.toDouble()
        val ret: Boolean = this.enabled && this.visible && ((dx * dx + dy * dy) < (this.size * this.size * 0.5 * 0.5))
        if (ret) {
            val dWheel: Int = if (0 < dx) {
                -1
            } else {
                1
            }
            knobAngle += dWheel * 18
            knobAngle %= 360
            onValueChange.accept(dWheel)
        }
        return ret
    }
}
