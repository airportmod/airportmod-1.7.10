package airportlight.modcore.gui.custombutton

import net.minecraft.client.gui.GuiButton
import kotlin.math.roundToInt


open class GuiButtonDouble(
    id: Int,
    val xPosD: Double,
    val yPosD: Double,
    val widthD: Double,
    val heightD: Double,
    displayString: String
) : GuiButton(id, xPosD.roundToInt(), yPosD.roundToInt(), widthD.roundToInt(), heightD.roundToInt(), displayString)