package airportlight.modcore.gui.custombutton

import airportlight.ModAirPortLight
import airportlight.util.Consumer
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class VSKnob(id: Int, x: Int, y: Int, val size: Int, private val onValueChange: Consumer) :
    GuiButton(id, x, y, 64 * size / 255, size, ""), IdoWheel {
    val buttonTextures: ResourceLocation = ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/ap_vs_knob.png")

    val offsetX: Double = 63.0
    val offsetY: Double = 127.5
    var scale: Double = 0.0
    var scale1: Double = 0.0

    init {
        scale = size.toDouble() / 255
        scale1 = 1 / scale
    }

    fun setEnabled(enabled: Boolean): VSKnob {
        this.enabled = enabled
        return this
    }

    private var knobAngle = 0

    override fun drawButton(p_146112_1_: Minecraft, p_146112_2_: Int, p_146112_3_: Int) {
        GL11.glPushMatrix()
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        this.field_146123_n =
            (p_146112_2_ >= this.xPosition - size) && (p_146112_3_ >= this.yPosition - size) && (p_146112_2_ < this.xPosition + size) && (p_146112_3_ < this.yPosition + size)
        GL11.glEnable(GL11.GL_BLEND)
        OpenGlHelper.glBlendFunc(770, 771, 1, 0)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        GL11.glTranslated(this.xPosition.toDouble(), this.yPosition.toDouble(), 0.0)
        GL11.glScaled(scale, scale, scale)
        p_146112_1_.textureManager.bindTexture(buttonTextures)
        val tofX = (knobAngle % 2) * 128
        GL11.glTranslated(-offsetX, -offsetY, 0.0)
        this.drawTexturedModalRect(0, 0, tofX, 0, 63, 255)
        if (!enabled) {
            drawGradientRect(0, 0, 63, 255, -0x5fefeff0, -0x5fafafb0)
        }
        GL11.glTranslated(offsetX, offsetY, 0.0)
        GL11.glScaled(scale1, scale1, scale1)
        GL11.glPopMatrix()
        this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_)
    }

    override fun doWheel(mouseX: Int, mouseY: Int, dWheel: Int) {
        if (enabled) {
            val dx: Double = this.xPosition - mouseX.toDouble()
            val dy: Double = this.yPosition - mouseY.toDouble()
            val dxy2: Double = dx * dx + dy * dy
            val range: Double = this.size * this.size * 0.5 * 0.5
            if (dxy2 < range) {
                var dW: Int = dWheel
                if (dW != 0) {
                    if (dW < 0) {
                        dW = -1
                    } else if (0 < dW) {
                        dW = 1
                    }
                    knobAngle += dW
                    knobAngle %= 2
                    onValueChange.accept(dW)
                }
            }
        }
    }

    override fun mousePressed(mc: Minecraft?, mouseX: Int, mouseY: Int): Boolean {
        val dx: Double = this.xPosition - mouseX.toDouble()
        val dy: Double = this.yPosition - mouseY.toDouble()
        val ret: Boolean = this.enabled && this.visible && ((dx * dx + dy * dy) < (this.size * this.size * 0.5 * 0.5))
        if (ret) {
            val dWheel: Int = if (0 < dy) {
                -1
            } else {
                1
            }
            knobAngle += dWheel
            knobAngle %= 2
            onValueChange.accept(dWheel)
        }
        return ret
    }
}
