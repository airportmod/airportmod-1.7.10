package airportlight.modcore.gui.custombutton;

import airportlight.ModAirPortLight;
import airportlight.util.Consumer;
import airportlight.util.Vec2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class SelectKnob extends GuiButton implements IdoWheel {
    private static final int NormalSize = 149;
    private static final double offset = (double) NormalSize / 2;
    private static final int[][] valuesAngle = {
            {-45, 45},
            {-45, 0, 45},
            {-45, 0, 45, 90},
            {-60, -30, 0, 30, 60},
            {-135, -90, -45, 0, 45, 90},
    };
    private static Vec2D[][] angleOffsets;

    static {
        double NSr = (NormalSize / 2.0) * 1.025;
        double NS_4 = (NormalSize / 4.0) * 1.025;
        double NSR2 = NS_4 * Math.sqrt(2);
        double NSR3 = NS_4 * Math.sqrt(3);
        angleOffsets = new Vec2D[][]{
                {new Vec2D(-NSR2, NSR2), new Vec2D(NSR2, NSR2)},
                {new Vec2D(-NSR2, NSR2), new Vec2D(0, NSr), new Vec2D(NSR2, NSR2)},
                {new Vec2D(-NSR2, NSR2), new Vec2D(0, NSr), new Vec2D(NSR2, NSR2), new Vec2D(NSr, 0)},
                {new Vec2D(-NSR3, NS_4), new Vec2D(-NS_4, NSR3), new Vec2D(0, NSr), new Vec2D(NS_4, NSR3), new Vec2D(NSR3, NS_4)},
                {new Vec2D(-NSR2, -NSR2), new Vec2D(-NSr, 0), new Vec2D(-NSR2, NSR2), new Vec2D(0, NSr), new Vec2D(NSR2, NSR2), new Vec2D(NSr, 0)}
        };
    }

    protected static final ResourceLocation buttonTextures = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/gui/selectknob.png");

    private final int size;
    private final double scale;
    private final double scale1;
    private int valIndex = 0;
    private final String[] values;
    private final int valuesLength;
    private final Consumer onValueChange;

    public SelectKnob(int id, int x, int y, int size, String[] values, int valIndex, Consumer onValueChange) {
        super(id, x, y, size, size, "");
        this.size = size;
        scale = (double) size / NormalSize;//255
        scale1 = 1 / scale;
        this.values = values;
        this.valuesLength = values.length;
        if (this.valuesLength < 2 || 6 < this.valuesLength) {
            throw new IllegalArgumentException("SelectKnob values length is Out of Range");
        }
        this.valIndex = valIndex;
        this.onValueChange = onValueChange;
    }

    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_) {
        GL11.glPushMatrix();
        FontRenderer fontrenderer = p_146112_1_.fontRenderer;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.field_146123_n = p_146112_2_ >= this.xPosition - size && p_146112_3_ >= this.yPosition - size && p_146112_2_ < this.xPosition + size && p_146112_3_ < this.yPosition + size;
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslated(this.xPosition, this.yPosition, 0);
        GL11.glScaled(scale, scale, scale);
        p_146112_1_.getTextureManager().bindTexture(buttonTextures);
        int KnobAngle = getKnobValueAngle(valIndex);
        GL11.glRotated(KnobAngle, 0, 0, 1);
        GL11.glTranslated(-offset, -offset, 0);
        this.drawTexturedModalRect(0, 0, 0, 0, NormalSize, NormalSize);
        GL11.glTranslated(offset, offset, 0);
        GL11.glRotated(-KnobAngle, 0, 0, 1);

        Vec2D offsetXY;
        int stringWidthOffset, stringHeightOffset;
        for (int i = 0; i < valuesLength; i++) {
            offsetXY = getKnobValueOffset(i);
            if (offsetXY.x < 0) {
                stringWidthOffset = fontrenderer.getStringWidth(values[i]);
            } else if (offsetXY.x == 0) {
                stringWidthOffset = fontrenderer.getStringWidth(values[i]) / 2;
            } else {
                stringWidthOffset = 0;
            }
            if (0 < offsetXY.y) {
                stringHeightOffset = fontrenderer.FONT_HEIGHT;
            } else if (0 == offsetXY.y) {
                stringHeightOffset = fontrenderer.FONT_HEIGHT / 2;
            } else {
                stringHeightOffset = 0;
            }
            GL11.glTranslated(offsetXY.x, -offsetXY.y, 0);
            GL11.glScaled(scale1, scale1, scale1);
            fontrenderer.drawString(values[i], -stringWidthOffset, -stringHeightOffset, -1);
            GL11.glScaled(scale, scale, scale);
            GL11.glTranslated(-offsetXY.x, offsetXY.y, 0);
        }
        GL11.glScaled(scale1, scale1, scale1);
        GL11.glPopMatrix();


        this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);
    }

    private Vec2D getKnobValueOffset(int value) {
        return angleOffsets[this.valuesLength - 2][value];
    }

    private int getKnobValueAngle(int value) {
        return valuesAngle[this.valuesLength - 2][value];
    }

    @Override
    public void doWheel(int mouseX, int mouseY, int dWeel) {
        if (enabled) {
            double dx = this.xPosition - mouseX;
            double dy = this.yPosition - mouseY;
            double dxy2 = dx * dx + dy * dy;
            double range = this.size * this.size * 0.5 * 0.5;
            if (dWeel < 0) {
                dWeel = -1;
            } else if (0 < dWeel) {
                dWeel = 1;
            }
            if (dWeel != 0 && dxy2 < range) {
                this.addValIndex(dWeel);
                onValueChange.accept(valIndex);
            }
        }
    }

    private void addValIndex(int i) {
        valIndex += i;
        if (valIndex < 0) {
            valIndex = 0;
        } else if (valuesLength <= valIndex) {
            valIndex = valuesLength - 1;
        }
    }

    public void setValIndex(int i) {
        valIndex = i;
        if (valIndex < 0) {
            valIndex = 0;
        } else if (valuesLength <= valIndex) {
            valIndex = valuesLength - 1;
        }
    }

    public int getValIndex() {
        return valIndex;
    }

    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        double dx = this.xPosition - mouseX;
        double dy = this.yPosition - mouseY;
        boolean ret = this.enabled && this.visible && (dx * dx + dy * dy) < (this.size * this.size * 0.5 * 0.5);
        if (ret) {
            if (0 < dx) {
                addValIndex(-1);
            } else {
                addValIndex(1);
            }
            onValueChange.accept(valIndex);
        }
        return ret;
    }
}
