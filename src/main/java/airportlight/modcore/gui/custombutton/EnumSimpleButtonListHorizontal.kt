package airportlight.modcore.gui.custombutton

import java.util.*

class EnumSimpleButtonListHorizontal<E : Enum<E>>(val id: Int, val x: Int, val y: Int, val with: Int, val height: Int = 20, enum: Class<E>) {
    val buttonList = ArrayList<EnumSimpleButton<E>>()

    init {
        for (value in EnumSet.allOf(enum)) {
            buttonList.add(EnumSimpleButton(id, x + (with + 10) * value.ordinal, y, with, value))
        }
    }

    fun selectButton(select: E) {
        for (button in buttonList) {
            button.enabled = button.value != select
        }
    }
}
