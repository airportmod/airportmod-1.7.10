package airportlight.modcore;

import airportlight.ModAirPortLight;
import airportlight.modcore.config.AirportConfig;
import airportlight.modsystem.ModelSwitcherDataBank;
import airportlight.util.IUseWeightModel;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;

public class ReUseModelDataBank implements IUseWeightModel {
    public static ReUseModelDataBank instance = new ReUseModelDataBank();
    public static IModelCustom CenterLineBody = null;
    public static IModelCustom EdgeLight = null;
    public static GroupObject EdgeLightBody = null;
    public static GroupObject EdgeLightLight = null;
    private static final ResourceLocation textureOrange = new ResourceLocation(ModAirPortLight.DOMAIN, "orange.png");
    public static boolean noninit = true;

    public static void ModelInit() {
        instance.readModel(AirportConfig.UseWeightModel);
        setDisplayList();
    }

    public static void reloadModels(boolean UseWeightModel) {
        instance.readModel(UseWeightModel);
    }


    @Override
    public void readModel(boolean UseWeightModel) {
        if (noninit) {
            if (UseWeightModel) {
                CenterLineBody = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/centerlights.obj"));
                EdgeLight = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/edgelight.obj"));
            } else {
                CenterLineBody = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightcenterlights.obj"));
                EdgeLight = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/lightedgelight.obj"));
            }
            for (GroupObject groupObject : ((WavefrontObject) EdgeLight).groupObjects) {
                if (groupObject.name.equals("Body")) {
                    EdgeLightBody = groupObject;
                } else if (groupObject.name.equals("Light")) {
                    EdgeLightLight = groupObject;
                }
            }
            noninit = false;
        }
    }

    public static void setDisplayList() {
        if (DisplayListIDs.CenterLineBody == -1) {
            DisplayListIDs.CenterLineBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.CenterLineBody, GL11.GL_COMPILE);
            CenterLineBody.renderAll();
            GL11.glEndList();
        }
        if (DisplayListIDs.EdgeBody == -1) {
            DisplayListIDs.EdgeBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.EdgeBody, GL11.GL_COMPILE);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureOrange);
            EdgeLightBody.render();
            GL11.glEndList();
        }
        if (DisplayListIDs.EdgeLightON == -1) {
            DisplayListIDs.EdgeLightON = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.EdgeLightON, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            EdgeLightLight.render(tessellator);
            tessellator.draw();
            GL11.glEndList();
        }
        if (DisplayListIDs.EdgeLightOFF == -1) {
            DisplayListIDs.EdgeLightOFF = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.EdgeLightOFF, GL11.GL_COMPILE);
            EdgeLightLight.render();
            GL11.glEndList();
        }
    }
}
