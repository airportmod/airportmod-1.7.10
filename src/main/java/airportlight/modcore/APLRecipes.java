package airportlight.modcore;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class APLRecipes {
    public APLRecipes() {
    }

    public void registerRecipes() {

        Item lamp = InstanceList.itemLamp;
        Item lens = InstanceList.itemHalfRedLens;
        //赤いランプ
        GameRegistry.addRecipe(new ItemStack(lamp, 4, 0),
                "###",
                "#R#",
                "#G#",
                '#', Blocks.glass,
                'R', new ItemStack(Items.dye, 1, 1),
                'G', Items.glowstone_dust
        );

        //白いランプ
        GameRegistry.addRecipe(new ItemStack(lamp, 4, 1),
                "###",
                "#W#",
                "#G#",
                '#', Blocks.glass,
                'W', new ItemStack(Items.dye, 1, 15),
                'G', Items.glowstone_dust
        );

        //緑のランプ
        GameRegistry.addRecipe(new ItemStack(lamp, 4, 2),
                "###",
                "#M#",
                "#G#",
                '#', Blocks.glass,
                'M', new ItemStack(Items.dye, 1, 2),
                'G', Items.glowstone_dust
        );

        //半赤レンズ
        GameRegistry.addRecipe(new ItemStack(lens, 4),
                "r",
                "#",
                'r', new ItemStack(Blocks.stained_glass, 1, 14),
                '#', Blocks.glass
        );

        Block PAPIL = InstanceList.blockPAPIL;
        Block PAPILC = InstanceList.blockPAPILC;
        Block PAPICR = InstanceList.blockPAPICR;
        Block PAPIR = InstanceList.blockPAPIR;

        //PAPI L
        GameRegistry.addRecipe(new ItemStack(PAPIL),
                "iii",
                "Lh#",
                '#', Blocks.glass,
                'L', new ItemStack(lamp, 1, 1),
                'h', lens,
                'i', Items.iron_ingot
        );

        //PAPI LC
        GameRegistry.addShapelessRecipe(new ItemStack(PAPILC),
                new ItemStack(PAPIL)
        );

        //PAPI CR
        GameRegistry.addShapelessRecipe(new ItemStack(PAPICR),
                new ItemStack(PAPILC)
        );

        //PAPI R
        GameRegistry.addShapelessRecipe(new ItemStack(PAPIR),
                new ItemStack(PAPICR)
        );

        //PAPI L
        GameRegistry.addShapelessRecipe(new ItemStack(PAPIL),
                new ItemStack(PAPIR)
        );

        //CenterLineLight
        GameRegistry.addRecipe(new ItemStack(InstanceList.blockRunwayCenterLineLight),
                "#L#",
                "iii",
                '#', Blocks.glass,
                'L', new ItemStack(lamp, 1, 1),
                'i', Items.iron_ingot
        );

        //EndLight
        GameRegistry.addRecipe(new ItemStack(InstanceList.blockEndLight),
                "gir",
                "iii",
                'g', new ItemStack(lamp, 1, 0),
                'r', new ItemStack(lamp, 1, 2),
                'i', Items.iron_ingot
        );
    }
}
