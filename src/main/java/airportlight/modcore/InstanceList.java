package airportlight.modcore;

import airportlight.blocks.facility.asde.BlockASDE;
import airportlight.blocks.facility.glideslope.GlideSlopeBlock;
import airportlight.blocks.facility.localizer.LocalizerBlock;
import airportlight.blocks.facility.pbb.BlockPBB;
import airportlight.blocks.facility.psr.BlockPSR;
import airportlight.blocks.light.aerodromebeacon.BlockAerodromeBeacon;
import airportlight.blocks.light.approachlight.ApproachLightBlock;
import airportlight.blocks.light.apronlighting.ApronLightingBlock;
import airportlight.blocks.light.apronlighting.lightair.BlockLightAir;
import airportlight.blocks.light.endlight.BlockEndLight;
import airportlight.blocks.light.obstaclelight.BlockObstacleLight;
import airportlight.blocks.light.overrunareaedgelight.BlockOverrunAreaEdgeLight;
import airportlight.blocks.light.papi.*;
import airportlight.blocks.light.runwaycenterlinelight.BlockRunwayCenterLineLight;
import airportlight.blocks.light.runwaydistancemarkerlight.BlockRunwayDistanceMarkerLight;
import airportlight.blocks.light.runwayedgelight.BlockRunwayEdgeLight;
import airportlight.blocks.light.runwaytouchdownzonelight.BlockRunwayTouchdownZoneLight;
import airportlight.blocks.light.taxiwaycenterlinelight.BlockTaxiwayCenterLineLight;
import airportlight.blocks.light.taxiwayedgelight.BlockTaxiwayEdgeLight;
import airportlight.blocks.light.winddirectionindicatorlight.BlockWindDirectionIndicatorLight;
import airportlight.blocks.markings.gloundsine.GroundSineBlock;
import airportlight.blocks.markings.guidepanel.BlockGuidePanel;
import airportlight.blocks.markings.runwayaimingpointmarkings.RunwayAimingPointMarkingsBlock;
import airportlight.blocks.markings.runwayholdpositionmarkings.RunwayHoldPositionMarkingsBlock;
import airportlight.blocks.markings.runwaynumber.BlockRunwayNumber;
import airportlight.blocks.markings.runwaythresholdmarkings.RunwayThresholdMarkingsBlock;
import airportlight.blocks.markings.runwaytouchdownzonemarkings.RunwayTouchdownZoneMarkingsBlock;
import airportlight.items.ItemHalfRedLens;
import airportlight.items.ItemLamp;
import airportlight.landingpoint.BlockLandingPoint;
import airportlight.modsystem.navigation.vordme.BlockVORDME;
import airportlight.radar.artsdisplay.ArtsDisplayBlock;
import airportlight.towingcar.ItemTowingCar;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class InstanceList {

    // facility
    public static Block blockASDE = new BlockASDE();
    public static Block blockGlideSlope = new GlideSlopeBlock();
    public static Block blockLocalizer = new LocalizerBlock();
    public static Block blockPBB = new BlockPBB();
    public static Block blockPSR = new BlockPSR();

    // light
    public static Block blockAerodromeBeacon = new BlockAerodromeBeacon();
    public static Block blockApproachLight = new ApproachLightBlock();
    public static Block blockEndLight = new BlockEndLight();
    public static Block blockLandingPoint = new BlockLandingPoint();
    public static Block blockObstacleLight = new BlockObstacleLight();
    public static Block blockOverrunAreaEdgeLight = new BlockOverrunAreaEdgeLight();
    public static Block blockPAPIAuto = new BlockPAPIAuto();
    public static Block blockPAPIL = new BlockPAPIL();
    public static Block blockPAPILC = new BlockPAPILC();
    public static Block blockPAPICR = new BlockPAPICR();
    public static Block blockPAPIR = new BlockPAPIR();
    public static Block blockRunwayCenterLineLight = new BlockRunwayCenterLineLight();
    public static Block blockRunwayEdgeLight = new BlockRunwayEdgeLight();
    public static Block blockRunwayTouchdownZoneLight = new BlockRunwayTouchdownZoneLight();
    public static Block blockRunwayDistanceMarkerLight = new BlockRunwayDistanceMarkerLight();
    public static Block blockTaxiwayCenterLineLight = new BlockTaxiwayCenterLineLight();
    public static Block blockTaxiwayEdgeLight = new BlockTaxiwayEdgeLight();
    public static Block blockVORDME = new BlockVORDME();
    public static Block blockApronLighting = new ApronLightingBlock();
    public static Block blockArtsDisplay = new ArtsDisplayBlock();
    public static Block blockLightAir = new BlockLightAir();
    public static Block blockWindDirectionIndicatorLight = new BlockWindDirectionIndicatorLight();


    // markings
    public static Block blockGroundSine = new GroundSineBlock();
    public static Block blockGuidePanel = new BlockGuidePanel();
    public static Block blockRunwayHoldPositionMarkings = new RunwayHoldPositionMarkingsBlock();
    public static Block blockRunwayNumber = new BlockRunwayNumber();
    public static Block blockRunwayThresholdMarkings = new RunwayThresholdMarkingsBlock();
    public static Block blockRunwayTouchdownZoneMarkingsBlock = new RunwayTouchdownZoneMarkingsBlock();
    public static Block blockRunwayAimingPointMarkings = new RunwayAimingPointMarkingsBlock();

    // item
    public static Item itemTowingCar = new ItemTowingCar();
    public static Item itemLamp = new ItemLamp();
    public static Item itemHalfRedLens = new ItemHalfRedLens();

    public static Item itemBlockRunwayCenterLineLight;
}
