package airportlight.modcore.normal;

import airportlight.ModAirPortLight;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public abstract class BlockAngleLightNormal extends BlockContainer {
    public BlockAngleLightNormal() {
        super(Material.rock);
        setCreativeTab(ModAirPortLight.AirPortLightTabs);
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack) {
        super.onBlockPlacedBy(world, x, y, z, entity, itemStack);
        Item item = itemStack.getItem();
        if (item instanceof ItemBlockAngleLight) {
            TileEntity tile = world.getTileEntity(x, y, z);
            if (tile instanceof TileAngleLightNormal) {
                int step = ((ItemBlockAngleLight) item).getStep(itemStack);
                ((TileAngleLightNormal) tile).setBaseAngleWithOffset(entity, InversionBaseAngle(), step);
            }
        }
    }

    public boolean InversionBaseAngle() {
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World p_149668_1_, int p_149668_2_, int p_149668_3_, int p_149668_4_) {
        return null;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public int getRenderType() {
        return -1;
    }


    @Override
    public final TileEntity createNewTileEntity(World world, int metadata) {
        return createNewAngleTileEntity(world, metadata);
    }

    protected abstract TileAngleLightNormal createNewAngleTileEntity(World world, int metadata);

}