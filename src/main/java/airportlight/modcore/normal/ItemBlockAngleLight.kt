package airportlight.modcore.normal

import net.minecraft.block.Block
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemBlock
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.ChatComponentText
import net.minecraft.world.World

class ItemBlockAngleLight(block: Block) : ItemBlock(block) {
    private val angleSettingTag = "angleSetting"
    private val steps: ArrayList<Int> = arrayListOf(1, 15, 45)

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    override fun onItemRightClick(itemStack: ItemStack, world: World, entityPlayer: EntityPlayer): ItemStack {
        if (itemStack.tagCompound == null) {
            itemStack.tagCompound = NBTTagCompound()
        }

        val step = itemStack.tagCompound.getByte(angleSettingTag).toInt()
        val nowIndex = steps.indexOf(step)
        var newIndex = nowIndex + 1
        if (newIndex >= steps.size) {
            newIndex = 0
        }
        val newStep = steps[newIndex].toByte()
        itemStack.tagCompound.setByte(angleSettingTag, newStep)
        if (world.isRemote) {
            entityPlayer.addChatComponentMessage(ChatComponentText("Angle Step Set : $newStep"))
        }
        return itemStack
    }

    fun getStep(itemStack: ItemStack): Int {
        if (itemStack.tagCompound == null) {
            itemStack.tagCompound = NBTTagCompound()
        }
        var step = itemStack.tagCompound.getByte(angleSettingTag).toInt()
        if (step == 0) {
            step = steps[steps.size - 1]
        }
        return step
    }
}