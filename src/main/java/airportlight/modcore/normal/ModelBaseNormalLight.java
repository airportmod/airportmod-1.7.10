package airportlight.modcore.normal;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import java.nio.FloatBuffer;

@SideOnly(Side.CLIENT)
public abstract class ModelBaseNormalLight<T extends TileEntity> extends ModelBaseNormal<T> {
    protected final IModelCustom modelREDL, modelLED;

    public ModelBaseNormalLight() {
        super();
        this.modelREDL = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "redl.obj"));
        this.modelLED = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/runwaycenterlightsled.obj"));
    }

    protected abstract void ModelBodyRender();

    protected static FloatBuffer toFloatBuffer(float[] values) {
        FloatBuffer floats = BufferUtils.createFloatBuffer(4).put(values);
        floats.flip();
        return floats;
    }

    public void render(int light, IModelCustom obj) {
        if (obj instanceof WavefrontObject) {
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(light);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) obj).groupObjects) {
                groupObject.render(tessellator);
            }
            tessellator.draw();
        } else {
            obj.renderAll();
        }
    }

    public void render(int light, IModelCustom obj, String name) {
        if (obj instanceof WavefrontObject) {
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(light);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) obj).groupObjects) {
                if (groupObject.name.equals(name)) {
                    groupObject.render(tessellator);
                }
            }
            tessellator.draw();
        } else {
            obj.renderPart(name);
        }
    }

    public void renderREDLON() {
        if (DisplayListIDs.REDLON == -1) {
            DisplayListIDs.REDLON = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.REDLON, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) this.modelREDL).groupObjects) {
                groupObject.render(tessellator);
            }
            tessellator.draw();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.REDLON);
    }

    public void renderLightON_LED() {
        if (DisplayListIDs.CenterLineLightLEDON == -1) {
            DisplayListIDs.CenterLineLightLEDON = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.CenterLineLightLEDON, GL11.GL_COMPILE);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawing(GL11.GL_TRIANGLES);
            tessellator.setBrightness(240);
            tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
            for (GroupObject groupObject : ((WavefrontObject) this.modelLED).groupObjects) {
                groupObject.render(tessellator);
            }
            tessellator.draw();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.CenterLineLightLEDON);
    }

    public void renderLightOFF_LED() {
        if (DisplayListIDs.CenterLineLightLEDOFF == -1) {
            DisplayListIDs.CenterLineLightLEDOFF = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.CenterLineLightLEDOFF, GL11.GL_COMPILE);
            this.modelLED.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.CenterLineLightLEDOFF);
    }
}
