package airportlight.modcore.normal

import airportlight.util.MathHelperAirPort
import airportlight.util.roundStepDeg
import net.minecraft.entity.EntityLivingBase
import net.minecraft.nbt.NBTTagCompound

open class TileAngleLightNormal : TileNormal() {
    private var baseAngleDeg = 0.0
    var backAngle = 180.0
        private set
    var baseAngle: Double
        get() = baseAngleDeg
        set(angDeg) {
            baseAngleDeg = MathHelperAirPort.wrapAngleTo360_double(angDeg)
            backAngle = MathHelperAirPort.wrapAngleTo360_double(angDeg + 180.0)
        }

    open fun setBaseAngleWithOffset(entityLivingBase: EntityLivingBase, inversionBaseAngle: Boolean, step: Int) {
        var angleOffset = 0
        if (inversionBaseAngle) {
            angleOffset = 180
        }
        baseAngle = (entityLivingBase.rotationYaw + angleOffset).roundStepDeg(step).toDouble()
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setDouble("baseAngleDeg", baseAngleDeg)
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        baseAngleDeg = nbt.getDouble("baseAngleDeg")
        backAngle = MathHelperAirPort.wrapAngleTo360_double(baseAngleDeg + 180)
    }
}
