package airportlight.modcore.normal

import airportlight.util.currentWorld
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer

abstract class OverlayGuiOpenMessage : IMessage {
    var entityID: Int = 0
        private set

    constructor()
    constructor(entity: Entity) : this(entity.entityId)
    constructor(entityID: Int) {
        this.entityID = entityID
    }


    override fun fromBytes(buf: ByteBuf) {
        this.entityID = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.entityID)
    }

    companion object : IMessageHandler<OverlayGuiOpenMessage, IMessage> {
        override fun onMessage(message: OverlayGuiOpenMessage, ctx: MessageContext): IMessage? {
            val entity = message.getEntity(ctx)
            if (ctx.side == Side.SERVER) {
                val player = ctx.serverHandler.playerEntity
                if (entity is Entity) {
                    message.openOverlayScreen(player, entity)
                }
            }
            return null
        }
    }

    protected fun getEntity(ctx: MessageContext): Entity? {
        return ctx.currentWorld.getEntityByID(entityID)
    }

    abstract fun openOverlayScreen(player: EntityPlayer, entity: Entity): Boolean
}
