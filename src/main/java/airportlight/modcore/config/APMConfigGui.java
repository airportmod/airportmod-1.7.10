package airportlight.modcore.config;

import airportlight.ModAirPortLight;
import cpw.mods.fml.client.config.GuiConfig;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;

public class APMConfigGui extends GuiConfig {
    public APMConfigGui(GuiScreen parent) {
        super(
                parent,
                new ConfigElement(AirportConfig.config.getCategory(AirportConfig.GENERAL)).getChildElements(),
                ModAirPortLight.MOD_ID, ModAirPortLight.MOD_ID, false, false,
                "Airport Light Mod"
        );
    }
}
