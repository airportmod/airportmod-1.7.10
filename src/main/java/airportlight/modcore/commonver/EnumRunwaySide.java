package airportlight.modcore.commonver;

public enum EnumRunwaySide {
    NON(0), L(1), LC(2), C(3), CR(4), R(5);
    public final int ID;

    EnumRunwaySide(int id) {
        this.ID = id;
    }

    public static EnumRunwaySide getEnum(String input) {
        for (EnumRunwaySide side : values()) {
            if (side.name().equals(input)) {
                return side;
            }
        }
        return NON;
    }

    public static EnumRunwaySide getEnum(int id) {
        for (EnumRunwaySide side : values()) {
            if (side.ID == id) {
                return side;
            }
        }
        return NON;
    }

    public String getDisplayString() {
        if (this == NON) {
            return "";
        } else {
            return name();
        }
    }
}
