package airportlight.modcore.commonver;

import java.util.HashMap;

public enum GuiID {
    TowingCarDriver(1),
    RunwayNumber(2),
    LandingPoint(3),
    NavSetting(4),
    VORDMESetting(5),
    GuidePanel(6),
    ApproachLight(7),
    Localizer(8),
    Arts(9),
    PBBDrive(10),
    DistanceMarker(11),
    GroundSine(12),
    Papi(13),
    PBB(14),
    RunwayThresholdMarkings(15),
    RunwayTouchdownZoneMarkings(16),
    RunwayHoldPositionMarkings(17),
    RunwayAimingPointMarkings(18),
    ObstacleLight(19),
    ;
    final int id;

    GuiID(int id) {
        this.id = id;
    }

    static private HashMap<Integer, GuiID> idMap;

    static public GuiID getById(int id) {
        if (idMap == null) {
            idMap = new HashMap<Integer, GuiID>();

            for (int i = 0, m = values().length; i < m; i++) {
                GuiID entry = values()[i];
                idMap.put(entry.id, entry);
            }
        }
        return idMap.get(id);
    }

    public int getID() {
        return this.id;
    }
}
