package airportlight.modcore.proxy;

public abstract class APLProxy {
    public void preInit() {
    }
    public void init() {
    }

    public void onRenderTickPost() {
    }

    public void registerRenderer() {
    }
}
