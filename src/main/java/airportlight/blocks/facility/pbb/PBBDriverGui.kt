package airportlight.blocks.facility.pbb

import airportlight.modcore.config.APMKeyConfig
import airportlight.modcore.gui.overlayGui.OverlayScreen
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiIngameMenu
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.input.Keyboard

class PBBDriverGui : OverlayScreen {
    private var pbbDriverSeat: PBBDriverSeat? = null
    private var player: EntityPlayer? = null
    private var tilePBB: TilePBB? = null

    constructor() : super() {
        allowUserInput = true
    }

    constructor(player: EntityPlayer) : super(player, player.ridingEntity as PBBDriverSeat) {
        this.player = player
        val seat = (player.ridingEntity as? PBBDriverSeat)

        this.tilePBB = seat?.tilePBB
        if (player.ridingEntity is PBBDriverSeat) {
            pbbDriverSeat = player.ridingEntity as PBBDriverSeat
        }
        allowUserInput = true
    }

    override fun initGui() {
        super.initGui()
        mc.mouseHelper.grabMouseCursor()
    }

    override fun drawWorldBackground(p_146270_1_: Int) {
        if (mc.theWorld != null) {
            //操作説明の背景
            drawGradientRect(width - 100, height - 170, width - 10, height - 40, -804253680, -804253680)
        } else {
            drawBackground(p_146270_1_)
        }
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, p_146976_2_: Int, p_146976_3_: Int) {
        //操作説明一覧
        val settings = Minecraft.getMinecraft().gameSettings
        fontRendererObj.drawString(
            "Move       : "
                    + Keyboard.getKeyName(settings.keyBindForward.keyCode) + ", "
                    + Keyboard.getKeyName(settings.keyBindBack.keyCode),
            width - 95, height - 160, -1, false
        )
        fontRendererObj.drawString(
            ("Strafing    : "
                    + Keyboard.getKeyName(settings.keyBindLeft.keyCode) + ", "
                    + Keyboard.getKeyName(settings.keyBindRight.keyCode)), width - 95, height - 150, -1, false
        )
        fontRendererObj.drawString(
            "Brake      : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake)),
            width - 95,
            height - 140,
            -1,
            false
        )

        fontRendererObj.drawString(
            "Head Rotate : "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadLeft)) + ", "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadRight)),
            width - 95,
            height - 120,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Head Height : "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadUp)) + ", "
                    + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadDown)),
            width - 95,
            height - 110,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Cam Reset  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)),
            width - 95,
            height - 70,
            -1,
            false
        )
        fontRendererObj.drawString(
            "Dismount   : " + Keyboard.getKeyName(settings.keyBindSneak.keyCode),
            width - 95,
            height - 60,
            -1,
            false
        )
    }

    override fun onGuiClosed() {
        //その他設定をリセット
        pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.Brake, 0)
    }

    override fun keyTyped(p_73869_1_: Char, p_73869_2_: Int) {
        if (p_73869_2_ == 1) {
            mc.displayGuiScreen(GuiIngameMenu()) //closeScreen();
            Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor()
            return
        }
        //移動などのMinecraft通常の処理を呼び出すための処置
        val settings = Minecraft.getMinecraft().gameSettings
        if (Keyboard.isKeyDown(settings.keyBindSneak.keyCode)) {
            player!!.isInvisible = false
            player!!.mountEntity(null)
            player!!.closeScreen()
            return
        }
        if (Keyboard.getEventKeyState()) {
            val keyID = Keyboard.getEventKey()
            //cameraの向きをリセット
            if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)) {
                pbbDriverSeat!!.dataFlagUpdateSync(PBBDataFlag.CameraReset, 1)
            }
        }
    }

    override fun updateScreen() {
        super.updateScreen()
        //各種操作ボタンを押しているかの情報を送信
        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Brake,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Left,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadLeft))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Right,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadRight))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Up,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadUp))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.Down,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_PBB_HeadDown))) {
                1
            } else {
                0
            }
        )

        pbbDriverSeat!!.dataFlagUpdateSync(
            PBBDataFlag.CameraReset,
            if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset))) {
                1
            } else {
                0
            }
        )
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
