package airportlight.blocks.facility.pbb

import airportlight.ModAirPortLight
import airportlight.modcore.normal.OverlayGuiOpenMessage
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import net.minecraft.client.Minecraft
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer

class PBBDriverOpenGuiMessage : OverlayGuiOpenMessage {

    @Suppress("unused")
    constructor()
    constructor(towingCar: PBBDriverSeat) : super(towingCar)
    constructor(entityID: Int) : super(entityID)


    override fun openOverlayScreen(player: EntityPlayer, entity: Entity): Boolean {
        return if (entity is PBBDriverSeat) {
            ModAirPortLight.overlayGuiSystem.overlayScreen = PBBDriverGui(player)
            true
        } else {
            false
        }
    }


    companion object : IMessageHandler<PBBDriverOpenGuiMessage, IMessage> {
        override fun onMessage(message: PBBDriverOpenGuiMessage, ctx: MessageContext): IMessage? {
            val entity = message.getEntity(ctx)
            val player = Minecraft.getMinecraft().thePlayer
            if (entity is Entity) {
                message.openOverlayScreen(player, entity)
            }
            return null
        }
    }
}
