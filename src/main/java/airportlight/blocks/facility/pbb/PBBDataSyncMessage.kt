package airportlight.blocks.facility.pbb

import airportlight.modcore.PacketHandlerAPM
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf

class PBBDataSyncMessage : TileEntityMessage, IMessageHandler<PBBDataSyncMessage, IMessage> {
    private var useLongType: Boolean = false
    private var resetPos: Boolean = false

    constructor() : super() {}
    constructor(tile: TilePBB) : super(tile) {
        this.useLongType = tile.useLongType
    }

    constructor(tile: TilePBB, resetPos: Boolean) : super(tile) {
        this.useLongType = tile.useLongType
        this.resetPos = resetPos
    }

    override fun read(buf: ByteBuf) {
        this.useLongType = buf.readBoolean()
        this.resetPos = buf.readBoolean()
    }

    override fun write(buf: ByteBuf) {
        buf.writeBoolean(this.useLongType)
        buf.writeBoolean(this.resetPos)
    }

    override fun onMessage(message: PBBDataSyncMessage, ctx: MessageContext): IMessage? {
        val tile = message.getTileEntity(ctx)
        if (tile is TilePBB) {
            tile.setData(message.useLongType)
            if (message.resetPos) {
                tile.resetPos()
            }
            if (ctx.side.isServer) {
                message.tile = tile
                PacketHandlerAPM.sendPacketAll(message)
            }
        }
        return null
    }
}
