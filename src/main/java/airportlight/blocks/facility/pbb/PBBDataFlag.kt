package airportlight.blocks.facility.pbb

/**
 * PBBDriverSeatの各種データ共有用
 */
enum class PBBDataFlag(val memberID: Int, val flagID: Int) {
    GuiOpen(0, 10),
    Brake(1, 11),
    Dismount(2, 12),
    CameraReset(3, 13),
    Left(4, 14),
    Right(5, 15),
    Up(6, 16),
    Down(7, 17);

    companion object {
        private val values: Array<PBBDataFlag> by lazy {
            arrayOf(GuiOpen, Brake, Dismount, CameraReset, Left, Right, Up, Down)
        }

        fun getFlagFromMemberID(memberID: Int): PBBDataFlag {
            return values[memberID]
        }
    }
}