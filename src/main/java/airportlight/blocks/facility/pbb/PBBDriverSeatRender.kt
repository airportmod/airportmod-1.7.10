package airportlight.blocks.facility.pbb

import net.minecraft.client.renderer.entity.RenderEntity
import net.minecraft.entity.Entity

/**
 * PBB操縦用座席のEntityが写り込まないようにするためのレンダー
 */
class PBBDriverSeatRender : RenderEntity() {
    //    val textureGray: ResourceLocation by lazy { ResourceLocation(ModAirPortLight.DOMAIN, "gray.png") }
    override fun doRender(
        p_76986_1_: Entity,
        x: Double,
        y: Double,
        z: Double,
        p_76986_8_: Float,
        p_76986_9_: Float
    ) {
//        GL11.glPushMatrix()
//        GL11.glTranslated(x, y + 0.5, z)
//        FMLClientHandler.instance().client.renderEngine.bindTexture(this.textureGray)
//        if (DisplayListIDs.Box == -1) {
//            DisplayListIDs.Box = GL11.glGenLists(1)
//            GL11.glNewList(DisplayListIDs.Box, GL11.GL_COMPILE)
//            val tessellator = Tessellator.instance
//            tessellator.startDrawingQuads()
//            tessellator.addVertex(-0.5, 0.0, -0.5)
//            tessellator.addVertex(0.5, 0.0, -0.5)
//            tessellator.addVertex(0.5, 0.0, 0.5)
//            tessellator.addVertex(-0.5, 0.0, 0.5)
//            tessellator.addVertex(-0.5, 0.5, 0.5)
//            tessellator.addVertex(0.5, 0.5, 0.5)
//            tessellator.addVertex(0.5, 0.5, -0.5)
//            tessellator.addVertex(-0.5, 0.5, -0.5)
//
//            tessellator.addVertex(0.5, 0.0, -0.5)
//            tessellator.addVertex(0.5, 0.5, -0.5)
//            tessellator.addVertex(0.5, 0.5, 0.5)
//            tessellator.addVertex(0.5, 0.0, 0.5)
//            tessellator.addVertex(-0.5, 0.0, 0.5)
//            tessellator.addVertex(-0.5, 0.5, 0.5)
//            tessellator.addVertex(-0.5, 0.5, -0.5)
//            tessellator.addVertex(-0.5, 0.0, -0.5)
//
//
//            tessellator.addVertex(0.5, 0.0, 0.5)
//            tessellator.addVertex(0.5, 0.5, 0.5)
//            tessellator.addVertex(-0.5, 0.5, 0.5)
//            tessellator.addVertex(-0.5, 0.0, 0.5)
//            tessellator.addVertex(-0.5, 0.0, -0.5)
//            tessellator.addVertex(-0.5, 0.5, -0.5)
//            tessellator.addVertex(0.5, 0.5, -0.5)
//            tessellator.addVertex(0.5, 0.0, -0.5)
//
//            tessellator.draw()
//            GL11.glEndList()
//        }
//        GL11.glCallList(DisplayListIDs.Box)
//        GL11.glPopMatrix()
    }
}
