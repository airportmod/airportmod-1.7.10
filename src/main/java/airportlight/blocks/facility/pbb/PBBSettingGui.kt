package airportlight.blocks.facility.pbb

import airportlight.modcore.PacketHandlerAPM.sendPacketServer
import airportlight.modcore.gui.ContainerAirPort
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.inventory.GuiContainer
import org.lwjgl.opengl.GL11

class PBBSettingGui : GuiContainer {

    private lateinit var tilePBB: TilePBB

    private var inputID = 0

    private var useLongTypeButton = arrayOfNulls<GuiButton>(2)
    private var useLongType = false

    constructor() : super(ContainerAirPort()) {}
    constructor(tilePBB: TilePBB) : super(ContainerAirPort()) {
        this.tilePBB = tilePBB
        useLongType = tilePBB.useLongType
    }

    override fun onGuiClosed() {
        super.onGuiClosed()
        val isChanged = tilePBB.setData(useLongType)
        if (isChanged) {
            sendPacketServer(PBBDataSyncMessage(tilePBB))
        }
    }

    override fun initGui() {
        super.initGui()
        val w2 = width / 2
        val h = height / 2 - 10
        useLongTypeButton[0] = GuiButton(20, w2 - 50, h - 30, 40, 20, "Short Size")
        useLongTypeButton[1] = GuiButton(21, w2 + 10, h - 30, 40, 20, "Long Size")
        if (useLongType) {
            useLongTypeButton[1]!!.enabled = false
        } else {
            useLongTypeButton[0]!!.enabled = false
        }
        buttonList.addAll(useLongTypeButton)
        buttonList.add(GuiButton(11, w2 - 30, h + 20, 60, 20, "Reset Position"))

        buttonList.add(GuiButton(10, w2 - 15, h + 65, 30, 20, "OK"))
    }

    override fun drawGuiContainerBackgroundLayer(p_146976_1_: Float, p_146976_2_: Int, p_146976_3_: Int) {}

    override fun drawGuiContainerForegroundLayer(p_146979_1_: Int, p_146979_2_: Int) {
        GL11.glPushMatrix()
        GL11.glTranslated((-guiLeft + width / 2).toDouble(), (-guiTop + height / 2).toDouble(), 0.0)
        GL11.glPopMatrix()
    }


    override fun keyTyped(eventChar: Char, keyID: Int) {
        if (keyID == 1) {
            mc.thePlayer.closeScreen()
            return
        }
        super.keyTyped(eventChar, keyID)
    }

    override fun actionPerformed(button: GuiButton) {
        if (button.id == 10) {
            tilePBB.setData(useLongType)
            sendPacketServer(PBBDataSyncMessage(tilePBB))
            mc.thePlayer.closeScreen()
        }
        if (button.id == 11) {
            tilePBB.setData(useLongType)
            sendPacketServer(PBBDataSyncMessage(tilePBB, true))
        }
        if (button.id == 20) {
            useLongTypeButton[0]!!.enabled = false
            useLongTypeButton[1]!!.enabled = true
            useLongType = false
            tilePBB.setData(useLongType)
            sendPacketServer(PBBDataSyncMessage(tilePBB))
        } else if (button.id == 21) {
            useLongTypeButton[0]!!.enabled = true
            useLongTypeButton[1]!!.enabled = false
            useLongType = true
            tilePBB.setData(useLongType)
            sendPacketServer(PBBDataSyncMessage(tilePBB))
        }
    }

    override fun doesGuiPauseGame(): Boolean {
        return false
    }
}
