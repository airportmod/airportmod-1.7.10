package airportlight.blocks.facility.pbb

import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf

/**
 * PBBDriverSeatの各種データ共有用
 */
class PBBDataFlagMessage : IMessage {
    var towingCarEntityID = 0
    var flag: PBBDataFlag? = null
    var data = 0

    constructor() {}
    constructor(towingCar: PBBDriverSeat, flag: PBBDataFlag?, data: Int) {
        towingCarEntityID = towingCar.entityId
        this.flag = flag
        this.data = data
    }

    override fun fromBytes(buf: ByteBuf) {
        towingCarEntityID = buf.readInt()
        flag = PBBDataFlag.getFlagFromMemberID(buf.readInt())
        data = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(towingCarEntityID)
        buf.writeInt(flag!!.memberID)
        buf.writeInt(data)
    }

    companion object : IMessageHandler<PBBDataFlagMessage, IMessage?> {
        override fun onMessage(message: PBBDataFlagMessage, ctx: MessageContext): IMessage? {
            val entity = ctx.serverHandler.playerEntity.worldObj.getEntityByID(message.towingCarEntityID)
            entity?.dataWatcher?.updateObject(message.flag!!.flagID, message.data)
            return null
        }
    }
}
