package airportlight.blocks.facility.glideslope;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormalLight;
import airportlight.modsystem.ModelSwitcherDataBank;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class GlideSlopeModel extends ModelBaseNormalLight<GlideSlopeTile> {
    protected IModelCustom model;
    protected final ResourceLocation textureModel;

    public GlideSlopeModel() {
        super();
        this.model = ModelSwitcherDataBank.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/glideslope_yukikaze.obj"));
        this.textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/glideslope_yukikaze.png");
    }

    @Override
    protected void ModelBodyRender() {
    }

    @Override
    public void render(@NotNull GlideSlopeTile tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        GL11.glRotated(-((GlideSlopeTile) tile).getBaseAngle(), 0.0, 1.0, 0.0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(textureModel);

        if (DisplayListIDs.GlideSlopeBody == -1) {
            DisplayListIDs.GlideSlopeBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.GlideSlopeBody, GL11.GL_COMPILE);
            model.renderAll();
            GL11.glEndList();
        }
        GL11.glCallList(DisplayListIDs.GlideSlopeBody);

        GL11.glPopMatrix();
    }
}
