package airportlight.blocks.facility.asde;

import airportlight.ModAirPortLight;
import airportlight.modcore.DisplayListIDs;
import airportlight.modcore.normal.ModelBaseNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class ASDEModel extends ModelBaseNormal<TileASDE> {
    protected final ResourceLocation texture;
    protected IModelCustom modelbody;

    public ASDEModel() {
        this.texture = new ResourceLocation(ModAirPortLight.DOMAIN, "textures/model/asde_yukikaze.png");
        this.modelbody = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/asde_yukikaze.obj"));
    }

    @Override
    public void render(@NotNull TileASDE tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);

        if (DisplayListIDs.ASDEBody == -1) {
            DisplayListIDs.ASDEBody = GL11.glGenLists(1);
            GL11.glNewList(DisplayListIDs.ASDEBody, GL11.GL_COMPILE);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.texture);
            this.modelbody.renderAll();
            GL11.glEndList();
        }

        GL11.glCallList(DisplayListIDs.ASDEBody);
        GL11.glPopMatrix();
    }


}
