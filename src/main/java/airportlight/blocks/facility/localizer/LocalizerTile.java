package airportlight.blocks.facility.localizer;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

public class LocalizerTile extends TileAngleLightNormal {
    public int lineCount = 15;
    public float linePitch = 1.63f;

    public LocalizerTile() {
    }

    public boolean setDatas(int lineCount, float linePitch) {
        if (this.lineCount != lineCount || this.linePitch != linePitch) {
            this.lineCount = lineCount;
            this.linePitch = linePitch;
            return true;
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(
                xCoord - 14,
                yCoord,
                zCoord - 14,
                xCoord + 15,
                yCoord + 2,
                zCoord + 15
        );
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setInteger("lineCount", lineCount);
        p_145841_1_.setFloat("linePitch", linePitch);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        lineCount = p_145839_1_.getInteger("lineCount");
        linePitch = p_145839_1_.getFloat("linePitch");
    }
}
