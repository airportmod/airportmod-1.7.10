package airportlight.blocks.light.runwaytouchdownzonelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class RunwayTouchdownZoneLightRenderer extends TileEntitySpecialRenderer {
    private final RunwayTouchdownZoneLightModel model = ModelSwitcherDataBank.registerModelClass(new RunwayTouchdownZoneLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileRunwayTouchdownZoneLight) {
            this.model.render((TileRunwayTouchdownZoneLight) tE, x, y, z);
        }
    }
}