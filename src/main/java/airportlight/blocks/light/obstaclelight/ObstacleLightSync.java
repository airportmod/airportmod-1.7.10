package airportlight.blocks.light.obstaclelight;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import org.jetbrains.annotations.NotNull;

public class ObstacleLightSync extends TileEntityMessage implements IMessageHandler<ObstacleLightSync, IMessage> {
    EnumObstacleLightColorSet lightMode;

    public ObstacleLightSync() {
        super();
    }

    public ObstacleLightSync(TileObstacleLight tile) {
        super(tile);
        this.lightMode = tile.lightMode;
    }

    @Override
    public void write(@NotNull ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("lightMode", this.lightMode.getMode());
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public void read(@NotNull ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.lightMode = EnumObstacleLightColorSet.Companion.getFromMode(tag.getInteger("lightMode"));
    }

    @Override
    public IMessage onMessage(ObstacleLightSync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileObstacleLight) {
            ((TileObstacleLight) tile).setDatas(message.lightMode);
            tile.markDirty();
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
