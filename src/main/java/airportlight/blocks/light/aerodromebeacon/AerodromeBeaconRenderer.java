package airportlight.blocks.light.aerodromebeacon;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class AerodromeBeaconRenderer extends TileEntitySpecialRenderer {
    private final AerodromeBeaconModel model = new AerodromeBeaconModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileAerodromeBeacon) {
            this.model.render((TileAerodromeBeacon) tE, x, y, z);
        }
    }

}
