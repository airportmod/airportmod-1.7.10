package airportlight.blocks.light.aerodromebeacon;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockAerodromeBeacon extends BlockNormal {
    public BlockAerodromeBeacon() {
        super();
        setBlockName("AerodromeBeacon");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":aerodrome");
        setBlockBounds(-0.5F, 0F, -0.5F, 1.5F, 9.0F, 1.5F);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox((double) p_149633_2_ -0.5, p_149633_3_, (double) p_149633_4_ -0.5, (double) p_149633_2_ + 1.5, (double) p_149633_3_ + 9.0, (double) p_149633_4_ + 1.5);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata) {
        return new TileAerodromeBeacon();
    }
}
