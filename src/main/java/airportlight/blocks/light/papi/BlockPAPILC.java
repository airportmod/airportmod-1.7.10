package airportlight.blocks.light.papi;

import airportlight.ModAirPortLight;
import airportlight.blocks.light.papi.TilePAPI.PAPIMode;
import airportlight.modcore.normal.TileAngleLightNormal;
import net.minecraft.world.World;

public class BlockPAPILC extends BlockPAPI{

    public BlockPAPILC() {
        super();
        setBlockTextureName(ModAirPortLight.DOMAIN + ":papilc");
        setBlockName("PAPILC");
    }

    //region ブロック情報関係
    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TilePAPI(PAPIMode.LC);
    }
}
