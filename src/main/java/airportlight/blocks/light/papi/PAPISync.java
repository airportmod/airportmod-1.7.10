package airportlight.blocks.light.papi;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class PAPISync extends TileEntityMessage implements IMessageHandler<PAPISync, IMessage> {
    float glideSlopeAng;
    float papiWideInterval;

    public PAPISync() {
        super();
    }

    public PAPISync(TilePAPI tile, float glideSlopeAng, float papiWideInterval) {
        super(tile);
        this.glideSlopeAng = glideSlopeAng;
        this.papiWideInterval = papiWideInterval;
    }

    @Override
    public void read(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);
        this.glideSlopeAng = tag.getFloat("glideSlopeAng");
        this.papiWideInterval = tag.getFloat("papiWideInterval");
    }

    @Override
    public void write(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setFloat("glideSlopeAng", this.glideSlopeAng);
        tag.setFloat("papiWideInterval", this.papiWideInterval);
        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(PAPISync message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TilePAPI) {
            ((TilePAPI) tile).setData(message.glideSlopeAng, message.papiWideInterval);
            if (ctx.side.isServer()) {
                message.setTile(tile);
                PacketHandlerAPM.sendPacketAll(message);
            }
        }
        return null;
    }
}
