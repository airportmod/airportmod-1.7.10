package airportlight.blocks.light.papi;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.TileAngleLightNormal;
import net.minecraft.world.World;

public class BlockPAPIAuto extends BlockPAPI {

    public BlockPAPIAuto() {
        super();
        setBlockTextureName(ModAirPortLight.DOMAIN + ":papi");
        setBlockName("PAPI");
    }

    //region ブロック情報関係
    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TilePAPI(TilePAPI.PAPIMode.Auto);
    }
}
