package airportlight.blocks.light.papi;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockPAPI extends BlockAngleLightNormal {

    public BlockPAPI() {
        super();
        setBlockTextureName(ModAirPortLight.DOMAIN + ":papi");
        setBlockName("PAPI");
        setBlockBounds(0.1F, 0F, 0.1F, 0.9F, 0.55F, 0.9F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TilePAPI) {
            player.openGui(ModAirPortLight.instance, GuiID.Papi.getID(), world, x, y, z);
            return true;
        }
        return false;
    }

    @Override
    public boolean InversionBaseAngle() {
        return true;
    }

    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TilePAPI(TilePAPI.PAPIMode.Auto);
    }
}
