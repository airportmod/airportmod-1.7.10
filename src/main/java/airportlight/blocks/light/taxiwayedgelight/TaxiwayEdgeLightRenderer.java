package airportlight.blocks.light.taxiwayedgelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class TaxiwayEdgeLightRenderer extends TileEntitySpecialRenderer {
    private final TaxiwayEdgeLightModel model = ModelSwitcherDataBank.registerModelClass(new TaxiwayEdgeLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileTaxiwayEdgeLight) {
            this.model.render((TileTaxiwayEdgeLight) tE, x, y, z);
        }
    }
}