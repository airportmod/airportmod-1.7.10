package airportlight.blocks.light.apronlighting.lightair

import airportlight.modcore.InstanceList
import airportlight.modcore.normal.TileNormal
import airportlight.util.Vec3I
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.nbt.NBTTagCompound

class TileLightAir : TileNormal() {
    var parent: Vec3I? = null

    fun check() {
        if (parent != null) {
            if (worldObj.blockExists(parent!!.x, parent!!.y, parent!!.z)) {
                val block = worldObj.getBlock(parent!!.x, parent!!.y, parent!!.z)
                if (block != InstanceList.blockApronLighting) {
                    worldObj.setBlockToAir(this.xCoord, this.yCoord, this.zCoord)
                }
            }
        }
    }

    override fun updateEntity() {
        if (parent != null) {
            if (worldObj.blockExists(parent!!.x, parent!!.y, parent!!.z)) {
                val block = worldObj.getBlock(parent!!.x, parent!!.y, parent!!.z)
                if (block != InstanceList.blockApronLighting) {
                    worldObj.setBlockToAir(this.xCoord, this.yCoord, this.zCoord)
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    override fun getMaxRenderDistanceSquared(): Double {
        return 0.0
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        nbt.setBoolean("parent", parent != null)
        if (parent != null) {
            nbt.setInteger("x", parent!!.x)
            nbt.setInteger("y", parent!!.y)
            nbt.setInteger("z", parent!!.z)
        }
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        if (nbt.getBoolean("parent")) {
            parent = Vec3I(
                nbt.getInteger("x"),
                nbt.getInteger("y"),
                nbt.getInteger("z")
            )
        }
    }
}