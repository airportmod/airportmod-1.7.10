package airportlight.blocks.light.apronlighting;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class ApronLightingBlock extends BlockAngleLightNormal {
    public ApronLightingBlock() {
        super();
        setBlockName("ApronLighting");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":apronlighting");
        setBlockBounds(-2.75F, 0F, -2.75F, 3.75F, 21.5F, 3.75F);
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack) {
        super.onBlockPlacedBy(world, x, y, z, entity, itemStack);
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof ApronLightingTile) {
            ((ApronLightingTile) tile).startSetLightBlocks(Math.toRadians(((ApronLightingTile) tile).getBaseAngle() + 90));
        }
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int p_149749_6_) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof ApronLightingTile) {
            ((ApronLightingTile) tile).breakLights();
        }
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
//        if(tile instanceof ApronLightingTile){
//            player.openGui(ModAirPortLight.instance, GuiID.ApronLighting.getID(), world, x, y, z);
//            return true;
//        }
        return false;
    }

    @Override
    public boolean InversionBaseAngle() {
        return true;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox(p_149633_2_, p_149633_3_, p_149633_4_, (double) p_149633_2_ + 1.0, (double) p_149633_3_ + 2.0, (double) p_149633_4_ + 1.0);
    }

    @Override
    public TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new ApronLightingTile();
    }
}
