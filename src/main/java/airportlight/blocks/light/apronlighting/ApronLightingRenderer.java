package airportlight.blocks.light.apronlighting;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class ApronLightingRenderer extends TileEntitySpecialRenderer {
    private final ApronLightingModel model = new ApronLightingModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof ApronLightingTile) {
            this.model.render((ApronLightingTile) tE, x, y, z);
        }
    }
}