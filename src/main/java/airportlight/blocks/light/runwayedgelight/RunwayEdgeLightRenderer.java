package airportlight.blocks.light.runwayedgelight;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class RunwayEdgeLightRenderer extends TileEntitySpecialRenderer {
    private final RunwayEdgeLightModel model = ModelSwitcherDataBank.registerModelClass(new RunwayEdgeLightModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileRunwayEdgeLight) {
            this.model.render((TileRunwayEdgeLight) tE, x, y, z);
        }
    }
}