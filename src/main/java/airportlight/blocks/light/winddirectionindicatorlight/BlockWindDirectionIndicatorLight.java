package airportlight.blocks.light.winddirectionindicatorlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockWindDirectionIndicatorLight extends BlockAngleLightNormal {
    public BlockWindDirectionIndicatorLight() {
        super();
        setBlockName("WindDirectionIndicatorLight");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":winddirectionindicatorlight");
        setBlockBounds(0F, 0F, 0F, 1F, 3.2F, 1F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        return true;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z) {
        return AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0, y + 1.0, z + 1.0);
    }

    @Override
    protected TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new TileWindDirectionIndicatorLight();
    }
}
