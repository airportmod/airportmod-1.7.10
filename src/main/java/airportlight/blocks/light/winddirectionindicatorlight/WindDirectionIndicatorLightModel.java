package airportlight.blocks.light.winddirectionindicatorlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

public class WindDirectionIndicatorLightModel extends AngleLightModelBase<TileWindDirectionIndicatorLight> {
    protected IModelCustom model;
    protected final ResourceLocation textureModel;

    public WindDirectionIndicatorLightModel() {
        super();
        this.model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/winddirectionindicatorlight.obj"));
        this.textureModel = new ResourceLocation(ModAirPortLight.DOMAIN, "orange-white.png");
    }

    @Override
    protected void ModelBodyRender() {
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureModel);
        model.renderOnly("base", "paul", "tee");
    }

    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        model.renderOnly("wing", "wing1", "wing2", "wing3", "wing4");
        boolean lightON = worldLight < 0.5;
        if (lightON) {
            render(180, this.model, "light");
        } else {
            this.model.renderPart("light");
        }
    }
}
