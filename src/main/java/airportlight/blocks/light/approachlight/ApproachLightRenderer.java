package airportlight.blocks.light.approachlight;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class ApproachLightRenderer extends TileEntitySpecialRenderer {
    private final ApproachLightModel model = new ApproachLightModel();

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof ApproachLightTile) {
            this.model.render((ApproachLightTile) tE, x, y, z);
        }
    }
}