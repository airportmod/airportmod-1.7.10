package airportlight.blocks.light.approachlight;

import airportlight.ModAirPortLight;
import airportlight.modcore.commonver.GuiID;
import airportlight.modcore.normal.BlockAngleLightNormal;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class ApproachLightBlock extends BlockAngleLightNormal {
    public ApproachLightBlock() {
        super();
        setBlockName("ApproachLight");
        setBlockTextureName(ModAirPortLight.DOMAIN + ":approachlight");
        setBlockBounds(0F, 0F, 0F, 1F, 2F, 1F);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if(tile instanceof ApproachLightTile){
            player.openGui(ModAirPortLight.instance, GuiID.ApproachLight.getID(), world, x, y, z);
            return true;
        }
        return false;
    }

    @Override
    public boolean InversionBaseAngle() {
        return true;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World p_149633_1_, int p_149633_2_, int p_149633_3_, int p_149633_4_) {
        return AxisAlignedBB.getBoundingBox(p_149633_2_, p_149633_3_, p_149633_4_, (double) p_149633_2_ + 1.0, (double) p_149633_3_ + 2.0, (double) p_149633_4_ + 1.0);
    }

    @Override
    public TileAngleLightNormal createNewAngleTileEntity(World world, int metadata) {
        return new ApproachLightTile();
    }
}
