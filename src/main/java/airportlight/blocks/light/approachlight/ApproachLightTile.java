package airportlight.blocks.light.approachlight;

import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;

public class ApproachLightTile extends TileAngleLightNormal {
    public int lineCount = 5;
    public float linePitch = 15f;

    public ApproachLightTile() {
    }

    @SideOnly(Side.CLIENT)
    public int lightValue(long systemTime, int lightNumber) {
        long TimeDiff = systemTime % 500;
        int flashTime = (int) ((((double) lightNumber / lineCount) * 500) % 500);
        int light = (int) (240 - Math.abs(TimeDiff - flashTime));
        if (light < 120) {
            light = 120;
        }
        return light;
    }

    public boolean setDatas(int lineCount, float linePitch){
        if(this.lineCount != lineCount || this.linePitch != linePitch){
            this.lineCount = lineCount;
            this.linePitch = linePitch;
            return true;
        }
        return false;
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        p_145841_1_.setInteger("lineCount", lineCount);
        p_145841_1_.setFloat("linePitch", linePitch);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        lineCount = p_145839_1_.getInteger("lineCount");
        linePitch = p_145839_1_.getFloat("linePitch");
    }
}
