package airportlight.blocks.markings.gloundsine;

import airportlight.ModAirPortLight;
import airportlight.blocks.markings.guidepanel.EnumColorSet;
import airportlight.font.lwjgfont.LWJGFont;
import airportlight.modcore.normal.AngleLightModelBase;
import airportlight.modcore.normal.TileAngleLightNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GroundSineModel extends AngleLightModelBase<GroundSineTile> {
    protected IModelCustom model;

    public GroundSineModel() {
        super();
        this.model = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/plate.obj"));
    }

    @Override
    protected void ModelBodyRender() {
    }


    @Override
    protected void renderAngleLight(EntityPlayer player, TileAngleLightNormal tile, double dist, float worldLight, double playerAng) {
        GroundSineTile groundSineTile = (GroundSineTile) tile;
        String text = groundSineTile.text;
        EnumColorSet color = groundSineTile.color;
        LWJGFont font = color.font;

        GL11.glTranslatef(groundSineTile.widthOffset, 0, -groundSineTile.heightOffset);

        FMLClientHandler.instance().getClient().renderEngine.bindTexture(color.texture);
        GL11.glScaled(groundSineTile.getWidth(), 1, groundSineTile.getHeight());
        this.model.renderAll();
        GL11.glScaled(1 / groundSineTile.getWidth(), 1, 1 / groundSineTile.getHeight());


        int textWidth = font.stringWidth(text);
        int textHeight = font.getLineHeight();
        float widthK = (groundSineTile.getWidth() * 0.8f) / textWidth;
        float heightK = (groundSineTile.getHeight() * 0.8f) / textHeight;
        float k = Math.min(widthK, heightK);
        float scaledWidth = textWidth * k;
        float scaledHeight = textHeight * k * 0.7f;

        try {
            GL11.glRotatef(90, -1, 0, 0);
            GL11.glTranslated(-scaledWidth / 2f, -scaledHeight / 2f, 0.02f);
            GL11.glScaled(k, k, 1);
            font.drawString(text, 0, 0, 0);
            GL11.glScaled(1 / k, 1 / k, 1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
