package airportlight.blocks.markings.runwaythresholdmarkings

import airportlight.ModAirPortLight
import airportlight.modcore.normal.ModelBaseNormal
import cpw.mods.fml.client.FMLClientHandler
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import org.lwjgl.opengl.GL11

class RunwayThresholdMarkingsModel : ModelBaseNormal<RunwayThresholdMarkingsTile>() {
    protected val textureWhite: ResourceLocation = ResourceLocation(ModAirPortLight.DOMAIN, "white.png")
    protected val model: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(ModAirPortLight.DOMAIN, "models/1-8_30_bar.obj"))

    override fun render(tile: RunwayThresholdMarkingsTile, x: Double, y: Double, z: Double) {
        GL11.glPushMatrix()
        GL11.glTranslated(x + 0.5, y, z + 0.5)
        val ang = tile.baseAngle.toInt()
        GL11.glRotated(-ang.toDouble(), 0.0, 1.0, 0.0)
        FMLClientHandler.instance().client.renderEngine.bindTexture(textureWhite)
        val lineCnt = when (tile.runwayWide) {
            EnumRunwayThresholdMarkingsWide.W15 -> 2
            EnumRunwayThresholdMarkingsWide.W25 -> 3
            EnumRunwayThresholdMarkingsWide.W30 -> 4
            EnumRunwayThresholdMarkingsWide.W45 -> 6
            EnumRunwayThresholdMarkingsWide.W60 -> 8
        }
        val a = when (tile.runwayWide) {
            EnumRunwayThresholdMarkingsWide.W15 -> 1.6
            EnumRunwayThresholdMarkingsWide.W25 -> 2.0
            EnumRunwayThresholdMarkingsWide.W30 -> 1.7
            EnumRunwayThresholdMarkingsWide.W45 -> 1.75
            EnumRunwayThresholdMarkingsWide.W60 -> 1.8
        }
        val interval = a + 1.8
        val innerInterval = a + 0.9

        GL11.glTranslated(innerInterval, 0.0, 0.0)
        model.renderAll()

        for (i in 1 until lineCnt) {
            GL11.glTranslated(interval, 0.0, 0.0)
            model.renderAll()
        }

        GL11.glTranslated(-interval * (lineCnt - 1) - innerInterval, 0.0, 0.0)

        GL11.glTranslated(-innerInterval, 0.0, 0.0)
        model.renderAll()

        for (i in 1 until lineCnt) {
            GL11.glTranslated(-interval, 0.0, 0.0)
            model.renderAll()
        }
        GL11.glTranslated(interval * (lineCnt - 1) + innerInterval, 0.0, 0.0)

        if (tile.lineLong > 30) {
            GL11.glTranslated(0.0, 0.0, -tile.lineLong + 30.0)

            GL11.glTranslated(innerInterval, 0.0, 0.0)
            model.renderAll()

            for (i in 1 until lineCnt) {
                GL11.glTranslated(interval, 0.0, 0.0)
                model.renderAll()
            }

            GL11.glTranslated(-interval * (lineCnt - 1) - innerInterval, 0.0, 0.0)

            GL11.glTranslated(-innerInterval, 0.0, 0.0)
            model.renderAll()

            for (i in 1 until lineCnt) {
                GL11.glTranslated(-interval, 0.0, 0.0)
                model.renderAll()
            }
            GL11.glTranslated(interval * (lineCnt - 1) + innerInterval, 0.0, 0.0)
        }

        GL11.glPopMatrix()
    }
}
