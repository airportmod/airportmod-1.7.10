package airportlight.blocks.markings.runwaytouchdownzonemarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.nbt.NBTTagCompound

class RunwayTouchdownZoneMarkingsSync : TileEntityMessage {
    lateinit var lineCnt: EnumRunwayTouchdownZoneMarkingsCount
    var centerInterval = 0f

    @Suppress("unused")
    constructor()

    constructor(tile: RunwayTouchdownZoneMarkingsTile, lineCnt: EnumRunwayTouchdownZoneMarkingsCount, centerInterval: Float) : super(tile) {
        this.lineCnt = lineCnt
        this.centerInterval = centerInterval
    }

    override fun read(buf: ByteBuf) {
        val tag = ByteBufUtils.readTag(buf)
        this.lineCnt = EnumRunwayTouchdownZoneMarkingsCount.getFromMode(tag.getInteger("lineCnt"))
        this.centerInterval = tag.getFloat("centerInterval")
    }

    override fun write(buf: ByteBuf) {
        val tag = NBTTagCompound()
        tag.setInteger("lineCnt", this.lineCnt.cnt)
        tag.setFloat("centerInterval", this.centerInterval)
        ByteBufUtils.writeTag(buf, tag)
    }

    companion object : IMessageHandler<RunwayTouchdownZoneMarkingsSync, IMessage> {
        override fun onMessage(message: RunwayTouchdownZoneMarkingsSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is RunwayTouchdownZoneMarkingsTile) {
                tile.setInfo(message.lineCnt, message.centerInterval)
                if (ctx.side.isServer) {
                    message.tile = tile
                    PacketHandlerAPM.sendPacketAll(message)
                }
            }
            return null
        }
    }
}
