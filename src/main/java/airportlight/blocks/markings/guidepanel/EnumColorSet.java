package airportlight.blocks.markings.guidepanel;

import airportlight.ModAirPortLight;
import airportlight.font.lwjgfont.LWJGFont;
import airportlight.font.robotobold_h30_1_0.RobotoBoldH30FontBlack;
import airportlight.font.robotobold_h30_1_0.RobotoBoldH30FontWhite;
import airportlight.font.robotobold_h30_1_0.RobotoBoldH30FontYellow;
import net.minecraft.util.ResourceLocation;

public enum EnumColorSet {
    Red_White(0, new ResourceLocation(ModAirPortLight.DOMAIN, "red.png"), new RobotoBoldH30FontWhite()),
    Yellow_Black(1, new ResourceLocation(ModAirPortLight.DOMAIN, "yellow.png"), new RobotoBoldH30FontBlack()),
    Black_Yellow(2, new ResourceLocation(ModAirPortLight.DOMAIN, "black.png"), new RobotoBoldH30FontYellow()),
    ;

    public final int mode;
    public final ResourceLocation texture;
    public final LWJGFont font;

    EnumColorSet(int mode, ResourceLocation texture, LWJGFont font) {
        this.mode = mode;
        this.texture = texture;
        this.font = font;
    }

    public static EnumColorSet getFromMode(int mode) {
        return values()[mode];
    }

}
