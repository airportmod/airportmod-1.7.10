package airportlight.blocks.markings.runwaynumber;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.modcore.gui.ContainerAirPort;
import airportlight.modcore.gui.custombutton.EnumSimpleButton;
import airportlight.modcore.gui.custombutton.EnumSimpleButtonListHorizontal;
import airportlight.modcore.gui.custombutton.StringInputGuiButton;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.util.Arrays;

public class RunwayNumberGui extends GuiContainer {
    private static final int biBaseAng = 30;
    private static final int biStrAng = 31;

    private final TileRunwayNumber tileRN;
    private EnumRunwaySide side;
    private EnumSimpleButtonListHorizontal<EnumRunwaySide> sideButton;
    private int inputID;
    private StringInputGuiButton buttonBaseAng;
    private StringInputGuiButton buttonStrAng;

    private String scrapString;
    private int tempBaseAng;
    private int tempStrAng;

    private final GuiButton[] use18mSizeButton = new GuiButton[2];
    private boolean use18mSize;

    public RunwayNumberGui(TileRunwayNumber tileRN) {
        super(new ContainerAirPort());
        this.tileRN = tileRN;
        this.tempBaseAng = (int) tileRN.getBaseAngle();
        this.tempStrAng = tileRN.strAng;
        this.side = tileRN.side;
        this.use18mSize = tileRN.use18mSize;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        boolean isChanged = this.tileRN.setInfo(this.tempBaseAng, this.tempStrAng, this.side, this.use18mSize);
        if (isChanged) {
            PacketHandlerAPM.sendPacketServer(new SyncRunwayNumberData(this.tileRN));
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        int w2 = this.width / 2;

        buttonBaseAng = new StringInputGuiButton(biBaseAng, w2 - 30, this.height / 2 - 75, 80, 20, String.valueOf((int) tileRN.getBaseAngle()));
        buttonStrAng = new StringInputGuiButton(biStrAng, w2 - 30, this.height / 2 - 50, 80, 20, String.valueOf(tileRN.strAng));

        this.buttonList.add(buttonBaseAng);
        this.buttonList.add(buttonStrAng);

        int h = this.height / 2 - 10;

        this.sideButton = new EnumSimpleButtonListHorizontal<EnumRunwaySide>(0, w2 - 120, h, 30, 20, EnumRunwaySide.class);

        for (Object button : sideButton.getButtonList()) {
            this.buttonList.add(button);
        }

        switchSide(this.side);

        use18mSizeButton[0] = new GuiButton(20, w2 - 50, h + 30, 40, 20, "9m Size");
        use18mSizeButton[1] = new GuiButton(21, w2 + 10, h + 30, 40, 20, "18m Size");

        if (this.use18mSize) {
            use18mSizeButton[1].enabled = false;
        } else {
            use18mSizeButton[0].enabled = false;
        }

        this.buttonList.addAll(Arrays.asList(use18mSizeButton));

        this.buttonList.add(new GuiButton(10, w2 - 15, h + 65, 30, 20, "OK"));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int p_146979_1_, int p_146979_2_) {
        //ScaledResolution sr = new ScaledResolution(this.mc, this.mc.displayWidth / 2, this.mc.displayHeight / 2);
        GL11.glPushMatrix();
        GL11.glTranslated(-this.guiLeft + this.width / 2, -this.guiTop + this.height / 2, 0);
        drawCenteredString(this.fontRendererObj, "Base Angle (0-360)", -80, -70, -1);
        drawCenteredString(this.fontRendererObj, "String Angle (0-36)", -80, -45, -1);
        GL11.glPopMatrix();
    }

    private void switchSide(EnumRunwaySide side) {
        this.sideButton.selectButton(side);
        this.side = side;
    }


    @Override
    protected void keyTyped(char eventChar, int keyID) {
        if (keyID == 1) {
            this.mc.thePlayer.closeScreen();
            return;
        }
        if (inputID == biBaseAng || inputID == biStrAng) {
            if (keyID == Keyboard.KEY_RETURN) {
                if (this.inputID == biBaseAng) {
                    if (scrapString.isEmpty()) {
                        this.tempBaseAng = 0;
                    } else {
                        this.tempBaseAng = Integer.parseInt(scrapString);
                    }
                } else {
                    if (scrapString.isEmpty()) {
                        this.tempStrAng = 0;
                    } else {
                        this.tempStrAng = Integer.parseInt(scrapString);
                    }
                }
            } else if (keyID == Keyboard.KEY_BACK) {
                if (0 < scrapString.length()) {
                    scrapString = scrapString.substring(0, scrapString.length() - 1);
                }
            } else {
                int strSize;
                if (this.inputID == biBaseAng) {
                    strSize = 3;
                } else {
                    strSize = 2;
                }
                if (scrapString.length() < strSize && Character.isDigit(eventChar)) {
                    scrapString += eventChar;
                }
            }
            if (this.inputID == biBaseAng) {
                this.buttonBaseAng.displayString = scrapString;
            } else {
                this.buttonStrAng.displayString = scrapString;
            }
        } else {
            super.keyTyped(eventChar, keyID);
        }
    }

    @Override
    public void actionPerformed(GuiButton button) {
        if (button.id == 0) {
            switchSide(((EnumSimpleButton<EnumRunwaySide>) button).getValue());
            return;
        }
        if (button.id == 10) {
            this.tileRN.setInfo(this.tempBaseAng, this.tempStrAng, this.side, this.use18mSize);

            PacketHandlerAPM.sendPacketServer(new SyncRunwayNumberData(this.tileRN));

            this.mc.thePlayer.closeScreen();
        }

        if (button.id == 20) {
            use18mSizeButton[0].enabled = false;
            use18mSizeButton[1].enabled = true;
            this.use18mSize = false;
        } else if (button.id == 21) {
            use18mSizeButton[0].enabled = true;
            use18mSizeButton[1].enabled = false;
            this.use18mSize = true;
        }

        if (button.id == biBaseAng || button.id == biStrAng) {
            EnterSettings();
            this.inputID = button.id;
            this.scrapString = "";
            button.displayString = "";
        }
    }

    private void EnterSettings() {
        if (this.inputID == biBaseAng) {
            if (scrapString.isEmpty()) {
                this.buttonBaseAng.displayString = String.valueOf((int) tileRN.getBaseAngle());
            } else {
                this.buttonBaseAng.displayString = scrapString;
            }
            this.tempBaseAng = Integer.parseInt(this.buttonBaseAng.displayString);
        } else if (this.inputID == biStrAng) {
            if (scrapString.isEmpty()) {
                this.buttonStrAng.displayString = String.valueOf(tileRN.strAng);
            } else {
                this.buttonStrAng.displayString = scrapString;
            }
            this.tempStrAng = Integer.parseInt(this.buttonStrAng.displayString);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
