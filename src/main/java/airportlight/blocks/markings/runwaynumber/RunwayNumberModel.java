package airportlight.blocks.markings.runwaynumber;

import airportlight.ModAirPortLight;
import airportlight.modcore.normal.ModelBaseNormal;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class RunwayNumberModel extends ModelBaseNormal<TileRunwayNumber> {
    protected final ResourceLocation textureWhite;
    protected final IModelCustom[] modelNumbers;
    protected final IModelCustom modelL;
    protected final IModelCustom modelC;
    protected final IModelCustom modelR;

    public RunwayNumberModel() {
        this.textureWhite = new ResourceLocation(ModAirPortLight.DOMAIN, "white.png");

        IModelCustom[] models = new IModelCustom[10];
        for (int index = 0; index < models.length; index++) {
            models[index] = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/numbers/" + index + ".obj"));
        }
        this.modelNumbers = models;
        this.modelL = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/numbers/l.obj"));
        this.modelC = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/numbers/c.obj"));
        this.modelR = AdvancedModelLoader.loadModel(new ResourceLocation(ModAirPortLight.DOMAIN, "models/numbers/r.obj"));
    }

    @Override
    public void render(@NotNull TileRunwayNumber tile, double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y, z + 0.5);
        int ang = (int) tile.getBaseAngle();
        GL11.glRotated(-ang, 0, 1, 0);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(this.textureWhite);

        if (tile.use18mSize) {
            GL11.glScaled(2, 2, 2);
        }

        GL11.glTranslated(-1, -0.099, 0);
        this.modelNumbers[tile.ten].renderAll();
        GL11.glTranslated(+5, 0, 0);
        this.modelNumbers[tile.one].renderAll();
        GL11.glTranslated(-4, 0, 12);

        String side = tile.side.getDisplayString();
        switch (side.length()) {
            case 1:
                GL11.glTranslated(1.5, 0, 0);
                renderString(side.charAt(0));
                break;
            case 2:
                GL11.glTranslated(-1, 0, 0);
                renderString(side.charAt(0));
                GL11.glTranslated(+5, 0, 0);
                renderString(side.charAt(1));
                break;
        }
        GL11.glPopMatrix();
    }

    private void renderString(char str) {
        switch (str) {
            case 'L':
                this.modelL.renderAll();
                break;
            case 'C':
                this.modelC.renderAll();
                break;
            case 'R':
                this.modelR.renderAll();
                break;
        }
    }
}
