package airportlight.blocks.markings.runwaynumber;

import airportlight.modcore.PacketHandlerAPM;
import airportlight.modcore.commonver.EnumRunwaySide;
import airportlight.util.TileEntityMessage;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

public class SyncRunwayNumberData extends TileEntityMessage implements IMessage, IMessageHandler<SyncRunwayNumberData, IMessage> {
    int x, y, z;
    TileRunwayNumber tile;
    public int baseAng = 0;
    public int strAng = 0;
    EnumRunwaySide side;
    boolean use18mSize = false;

    public SyncRunwayNumberData() {
        super();
    }

    public SyncRunwayNumberData(TileRunwayNumber tile) {
        super(tile);
        this.tile = tile;
        this.baseAng = (int) tile.getBaseAngle();
        this.strAng = tile.strAng;
        this.side = tile.side;
        this.use18mSize = tile.use18mSize;
    }

    @Override
    public void read(ByteBuf buf) {
        this.baseAng = buf.readInt();
        this.strAng = buf.readInt();
        this.side = EnumRunwaySide.getEnum(ByteBufUtils.readUTF8String(buf));
        this.use18mSize = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeInt(this.baseAng);
        buf.writeInt(this.strAng);
        ByteBufUtils.writeUTF8String(buf, this.side.name());
        buf.writeBoolean(this.use18mSize);
    }

    @Override
    public IMessage onMessage(SyncRunwayNumberData message, MessageContext ctx) {
        TileEntity tile = message.getTileEntity(ctx);
        if (tile instanceof TileRunwayNumber) {
            ((TileRunwayNumber) tile).setInfo(message.baseAng, message.strAng, message.side, message.use18mSize);
            ((TileRunwayNumber) tile).side = message.side;
            if (ctx.side.isServer()) {
                PacketHandlerAPM.sendPacketAll(new SyncRunwayNumberData((TileRunwayNumber) tile));
            }
        }
        return null;
    }
}
