package airportlight.blocks.markings.runwayholdpositionmarkings

import airportlight.modcore.PacketHandlerAPM
import airportlight.util.TileEntityMessage
import cpw.mods.fml.common.network.ByteBufUtils
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import io.netty.buffer.ByteBuf
import net.minecraft.nbt.NBTTagCompound

class RunwayHoldPositionMarkingsSync : TileEntityMessage {
    lateinit var lineType: EnumRunwayHoldPositionMarkingsType
    var lineWide = 0

    @Suppress("unused")
    constructor()

    constructor(tile: RunwayHoldPositionMarkingsTile, lineType: EnumRunwayHoldPositionMarkingsType, lineWide: Int) : super(tile) {
        this.lineType = lineType
        this.lineWide = lineWide
    }

    override fun read(buf: ByteBuf) {
        val tag = ByteBufUtils.readTag(buf)
        this.lineType = EnumRunwayHoldPositionMarkingsType.getFromMode(tag.getInteger("lineType"))
        this.lineWide = tag.getInteger("lineWide")
    }

    override fun write(buf: ByteBuf) {
        val tag = NBTTagCompound()
        tag.setInteger("lineType", this.lineType.type)
        tag.setInteger("lineWide", this.lineWide)
        ByteBufUtils.writeTag(buf, tag)
    }

    companion object : IMessageHandler<RunwayHoldPositionMarkingsSync, IMessage> {
        override fun onMessage(message: RunwayHoldPositionMarkingsSync, ctx: MessageContext): IMessage? {
            val tile = message.getTileEntity(ctx)
            if (tile is RunwayHoldPositionMarkingsTile) {
                tile.setInfo(message.lineType, message.lineWide)
                if (ctx.side.isServer) {
                    message.tile = tile
                    PacketHandlerAPM.sendPacketAll(message)
                }
            }
            return null
        }
    }
}

