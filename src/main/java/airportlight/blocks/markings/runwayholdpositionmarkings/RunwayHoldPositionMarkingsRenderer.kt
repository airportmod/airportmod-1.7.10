package airportlight.blocks.markings.runwayholdpositionmarkings

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity

class RunwayHoldPositionMarkingsRenderer : TileEntitySpecialRenderer() {
    val model: RunwayHoldPositionMarkingsModel = RunwayHoldPositionMarkingsModel()
    val model2: RunwayHoldPositionMarkingsModel2 = RunwayHoldPositionMarkingsModel2()

    override fun renderTileEntityAt(tile: TileEntity, x: Double, y: Double, z: Double, tick: Float) {
        // モデルの表示
        if (tile is RunwayHoldPositionMarkingsTile) {
            when (tile.lineType) {
                EnumRunwayHoldPositionMarkingsType.RunwayHoldPosition -> this.model.render(tile, x, y, z)
                EnumRunwayHoldPositionMarkingsType.ILSHoldPosition -> this.model2.render(tile, x, y, z)
            }
        }
    }
}
