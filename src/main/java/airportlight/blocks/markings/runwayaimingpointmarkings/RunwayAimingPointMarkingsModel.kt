package airportlight.blocks.markings.runwayaimingpointmarkings

import airportlight.ModAirPortLight
import airportlight.modcore.normal.ModelBaseNormal
import cpw.mods.fml.client.FMLClientHandler
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import org.lwjgl.opengl.GL11

class RunwayAimingPointMarkingsModel : ModelBaseNormal<RunwayAimingPointMarkingsTile>() {
    protected val textureWhite: ResourceLocation = ResourceLocation(ModAirPortLight.DOMAIN, "white.png")
    protected val model: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(ModAirPortLight.DOMAIN, "models/plate.obj"))

    override fun render(tile: RunwayAimingPointMarkingsTile, x: Double, y: Double, z: Double) {
        val lineLong = tile.lineLong.long
        val lineWide = tile.lineWide
        val innerInterval = tile.centerInterval / 2.0 + lineWide / 2.0

        GL11.glPushMatrix()
        GL11.glTranslated(x + 0.5, y + 0.05, z + 0.5)
        val ang = tile.baseAngle.toInt()
        GL11.glRotated(-ang.toDouble(), 0.0, 1.0, 0.0)
        FMLClientHandler.instance().client.renderEngine.bindTexture(textureWhite)

        GL11.glTranslated(0.0, 0.0, -lineLong / 2.0)

        GL11.glTranslated(innerInterval, 0.0, 0.0)
        GL11.glScalef(lineWide, 1.0f, lineLong)
        model.renderAll()
        GL11.glScalef(1 / lineWide, 1.0f, 1 / lineLong)
        GL11.glTranslated(-innerInterval * 2, 0.0, 0.0)
        GL11.glScalef(lineWide, 1.0f, lineLong)
        model.renderAll()
        GL11.glScalef(1 / lineWide, 1.0f, 1 / lineLong)
        GL11.glTranslated(innerInterval, 0.0, 0.0)

        GL11.glPopMatrix()
    }
}

