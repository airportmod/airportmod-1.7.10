package airportlight.util;

import net.minecraft.util.MathHelper;

public class MathHelperAirPort {
    /**
     * the angle is reduced to an angle between -PI and +PI by mod, and a PI*2 check
     */
    public static double wrapAngleToPI_double(double p_76138_0_) {
        p_76138_0_ %= Math.PI * 2;

        if (p_76138_0_ >= Math.PI) {
            p_76138_0_ -= Math.PI * 2;
        }

        if (p_76138_0_ < -Math.PI) {
            p_76138_0_ += Math.PI * 2;
        }

        return p_76138_0_;
    }

    public static double wrapAngleTo360_double(double d) {
        d %= 360;

        if (d < 0) {
            d += 360;
        }

        return d;
    }

    public static int floor360(int ang36) {
        int ret = ang36 % 360;
        if (ret < 0) {
            ret += 360;
        }
        if (ret == 0) {
            ret = 360;
        }
        return ret;
    }

    public static int floor360(float ang36) {
        return floor360(MathHelper.floor_double(ang36));
    }

    public static int floor360(double ang36) {
        return floor360(MathHelper.floor_double(ang36));
    }

    public static int parseAircraftAng(Vec2D vec2D) {
        return floor360(-Math.toDegrees(Math.atan2(-vec2D.y, vec2D.x)) + 90);
    }
}
