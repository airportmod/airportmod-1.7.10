package airportlight.util;

import net.minecraft.entity.Entity;

import java.io.Serializable;

public class Vec3D implements Serializable {
    public final double x, y, z;

    public Vec3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vec3D(Entity entity) {
        this.x = entity.posX;
        this.y = entity.posY;
        this.z = entity.posZ;
    }

    @Override
    public int hashCode() {
        return (int) x << 8 + (int) y << 4 + (int) z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vec3D) {
            Vec3D objI = (Vec3D) obj;
            return this.x == objI.x && this.y == objI.y && this.z == objI.z;
        }
        return false;
    }

    public Vec3D minus(Vec3D vec) {
        double x = this.x - vec.x;
        double y = this.y - vec.y;
        double z = this.z - vec.z;
        return new Vec3D(x, y, z);
    }

    public Vec2D toVecXZ2D() {
        return new Vec2D(this.x, this.z);
    }

    public double sqrt() {
        return Math.sqrt(x * x + y * y * +z * z);
    }

    public static final Vec3D ORIGIN = new Vec3D(0.0, 0.0, 0.0);
}
