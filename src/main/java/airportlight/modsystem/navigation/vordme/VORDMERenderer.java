package airportlight.modsystem.navigation.vordme;

import airportlight.modsystem.ModelSwitcherDataBank;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

public class VORDMERenderer extends TileEntitySpecialRenderer {
    private final VORDMEModel model = ModelSwitcherDataBank.registerModelClass(new VORDMEModel());

    @Override
    public void renderTileEntityAt(TileEntity tE, double x, double y, double z, float tick) {
        // モデルの表示
        if (tE instanceof TileVORDME) {
            this.model.render((TileVORDME) tE, x, y, z);
        }
    }
}
