package airportlight.modsystem.navigation.navsetting

import airportlight.modsystem.navigation.frequency.FrequencyID
import net.minecraft.entity.Entity

object NavSettingSer {
    var useNavFreqs = HashMap<Entity, FrequencyID>()
    var navFreqActiveID = FrequencyID(0)
}