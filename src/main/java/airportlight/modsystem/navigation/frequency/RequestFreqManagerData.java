package airportlight.modsystem.navigation.frequency;

import airportlight.modsystem.navigation.ils.NavFreqManager;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class RequestFreqManagerData implements IMessageHandler<RequestFreqManagerData, IMessage>, IMessage {
    public RequestFreqManagerData() {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    @Override
    public IMessage onMessage(RequestFreqManagerData message, MessageContext ctx) {
        NavFreqManager.Companion.sendUseDataToPlayer(ctx.getServerHandler().playerEntity);
        return null;
    }
}
