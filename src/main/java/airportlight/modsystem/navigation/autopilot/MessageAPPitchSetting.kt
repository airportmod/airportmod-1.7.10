package airportlight.modsystem.navigation.autopilot

import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft

class MessageAPPitchSetting : IMessage, IMessageHandler<MessageAPPitchSetting, IMessage> {
    var pitchMode: EnumPitchMode =
        EnumPitchMode.ALTHOLD
    var altitude: Int = 150

    constructor()

    constructor(mode: EnumPitchMode, altitude: Int) {
        this.pitchMode = mode
        this.altitude = altitude
    }

    override fun fromBytes(buf: ByteBuf) {
        this.pitchMode =
            EnumPitchMode.getFromMode(buf.readInt())
        this.altitude = buf.readInt()
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.pitchMode.mode)
        buf.writeInt(this.altitude)
    }

    override fun onMessage(message: MessageAPPitchSetting, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val pilot = ctx.serverHandler.playerEntity
            val aircraft = ParentEntityGetter.getParent(pilot)
            AutopilotManager.startAutopilot(aircraft, pilot)
            val apSettingEntity = AutopilotManager.apSettings[aircraft]
            if (apSettingEntity != null) {
                apSettingEntity.pitchMode = message.pitchMode
                apSettingEntity.altitudeActive = message.altitude
                apSettingEntity.update = true
                return MessageAPPitchSetting(apSettingEntity.pitchMode, apSettingEntity.altitudeActive)
            }
            return null
        } else {
            //Client side
            val player = Minecraft.getMinecraft().thePlayer
            AutopilotSettingCli._pitchMode = message.pitchMode
            AutopilotSettingCli._altitudeActive = message.altitude

            AutopilotSettingCli.update = true
        }
        return null
    }
}
