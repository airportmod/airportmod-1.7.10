package airportlight.modsystem.navigation.autopilot

import airportlight.modsystem.navigation.navsetting.NavSettingSer
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayerMP

object AutopilotManager {
    val apSettings = HashMap<Entity, AutopilotSettingEntity>()

    @JvmStatic
    fun getAPpSetting(aircraft: Entity): AutopilotSettingEntity? {
        return apSettings[aircraft]
    }

    @JvmStatic
    fun startAutopilot(aircraft: Entity, player: EntityPlayerMP) {
        if (apSettings.containsKey(aircraft)) {
            return
        }
        apSettings[aircraft] = AutopilotSettingEntity(aircraft, player)
    }

    fun engage(aircraft: Entity, player: EntityPlayerMP) {
        if (!apSettings.containsKey(aircraft)) {
            startAutopilot(aircraft, player)
        }
        val apSP = apSettings[aircraft]
        if (apSP != null) {
            if (apSP._engagedFlag) {
                return
            } else if (apSP.carrier != null) {
                return
            }
            val carrier: EntityAutopilotCarrier

            if (aircraft is EntityAutopilotCarrier) {
                throw IllegalStateException("Already Start AP: $aircraft")
            } else {
                carrier = EntityAutopilotCarrier(aircraft.worldObj, apSP)
                carrier.setPosition(aircraft.posX, aircraft.posY - 0.05, aircraft.posZ)
            }

            apSP.carrier = carrier
            aircraft.worldObj.spawnEntityInWorld(carrier)
            aircraft.mountEntity(carrier)
            carrier.carryingEntity = aircraft
            carrier.setVelHorizontal(aircraft)

            NavSettingSer.useNavFreqs[player]?.let { NavSettingSer.useNavFreqs[aircraft] = it }
            apSP._engagedFlag = true
        }
    }

    fun disEngage(aircraft: Entity) {
        if (apSettings.containsKey(aircraft)) {
            aircraft.mountEntity(null)
            val carrier = apSettings[aircraft]?.carrier
            if (carrier != null) {
                aircraft.setPosition(carrier.posX, carrier.posY, carrier.posZ)
            }
            apSettings[aircraft]?.carrier = null
            apSettings[aircraft]?._engagedFlag = false
        }
    }
}
