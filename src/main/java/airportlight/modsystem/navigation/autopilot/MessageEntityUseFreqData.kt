package airportlight.modsystem.navigation.autopilot

import airportlight.modsystem.navigation.ClientNavSetting
import airportlight.modsystem.navigation.EnumNavMode
import airportlight.modsystem.navigation.frequency.FrequencyID
import airportlight.modsystem.navigation.navsetting.NavSettingCli
import airportlight.util.ParentEntityGetter
import cpw.mods.fml.common.network.simpleimpl.IMessage
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler
import cpw.mods.fml.common.network.simpleimpl.MessageContext
import cpw.mods.fml.relauncher.Side
import io.netty.buffer.ByteBuf

class MessageEntityUseFreqData : IMessage, IMessageHandler<MessageEntityUseFreqData, IMessage> {
    private lateinit var freqActive: FrequencyID
    private lateinit var freqStandby: FrequencyID

    constructor()

    constructor(
        freqActive: FrequencyID?,
        freqStandby: FrequencyID?
    ) {
        this.freqActive = freqActive ?: FrequencyID(-1)
        this.freqStandby = freqStandby ?: FrequencyID(-1)
    }

    override fun fromBytes(buf: ByteBuf) {
        this.freqActive = FrequencyID(buf.readInt())
        this.freqStandby = FrequencyID(buf.readInt())
    }

    override fun toBytes(buf: ByteBuf) {
        buf.writeInt(this.freqActive.ID)
        buf.writeInt(this.freqStandby.ID)
    }

    override fun onMessage(message: MessageEntityUseFreqData, ctx: MessageContext): IMessage? {
        if (ctx.side == Side.SERVER) {
            //Server side
            val player = ctx.serverHandler.playerEntity
            val apSetting = AutopilotManager.apSettings[ParentEntityGetter.getParent(player)]
            apSetting?.freqActive = if (message.freqActive.ID < 0) {
                null
            } else {
                message.freqActive
            }
            apSetting?.freqStandby = if (message.freqStandby.ID < 0) {
                null
            } else {
                message.freqStandby
            }
            return null
        } else {
            NavSettingCli._navFreqActiveID = if (message.freqActive.ID < 0) {
                ClientNavSetting.navMode = EnumNavMode.OFF
                FrequencyID(0)
            } else {
                message.freqActive
            }
            NavSettingCli._navFreqStandByID = if (message.freqStandby.ID < 0) {
                ClientNavSetting.navMode = EnumNavMode.OFF
                FrequencyID(0)
            } else {
                message.freqStandby
            }
        }
        return null
    }
}
