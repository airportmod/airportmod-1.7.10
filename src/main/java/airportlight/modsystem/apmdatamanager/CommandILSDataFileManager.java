package airportlight.modsystem.apmdatamanager;

import airportlight.modsystem.navigation.NavRadioData;
import airportlight.modsystem.navigation.frequency.FrequencyID;
import airportlight.modsystem.navigation.ils.CommandILSManager;
import airportlight.modsystem.navigation.ils.ILSData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.DimensionManager;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CommandILSDataFileManager {
    static String newDefaultFileName = "ILSList.xml";
    static String defaultFileName = "CommandILSList.xml";

    public static void LoadILSData() {
        // 1. DocumentBuilderFactoryのインスタンスを取得する
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // 2. DocumentBuilderのインスタンスを取得する
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        // 3. DocumentBuilderにXMLを読み込ませ、Documentを作る
        Document document = null;
        try {
            File oldDir = new File(DimensionManager.getCurrentSaveRootDirectory(), "AirpotMod/");
            if (oldDir.exists()) {
                oldDir.renameTo(defaultSaveDir);
            }

            defaultSaveDir.mkdirs();

            File dataFile = new File(defaultSaveDir, newDefaultFileName);
            if (!dataFile.exists()) {
                File oldDataFile = new File(defaultSaveDir, oldSaveFileName().getName());
                if (oldDataFile.exists()) {
                    dataFile = oldDataFile;
                } else {
                    writeILSData(emptyData);
                }
            }
            assert builder != null;
            document = builder.parse(dataFile);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        assert document != null;
        // 4. Documentから、ルート要素(Data)を取得する
        Element tagTree = document.getDocumentElement();

        //5. File Format Version を取得する
        NodeList formatVersionTag = tagTree.getElementsByTagName("FileFormat");
        int formatVersion = 0;
        if (formatVersionTag.getLength() > 0) {
            formatVersion = Integer.parseInt(((Element) formatVersionTag.item(0)).getAttribute("Version"));
        }

        // 5. ILSList配下にある、ILS要素を取得する
        NodeList ilss;
        if (formatVersion >= 1) {
            ilss = ((Element) tagTree.getElementsByTagName("ILSList").item(0)).getElementsByTagName("ILS");
        } else {
            ilss = tagTree.getElementsByTagName("ILS");
        }


        HashMap<String, NavRadioData> centerList = new HashMap<String, NavRadioData>();

        // 6. 取得したnavData要素でループする
        for (int i = 0; i < ilss.getLength(); i++) {
            // 7. ILS要素をElementにキャストする
            Element navData = (Element) ilss.item(i);

            // 8. ILS要素の属性値と、テキストノードの値を取得する
            String posX = navData.getAttribute("posX");
            String posY = navData.getAttribute("posY");
            String posZ = navData.getAttribute("posZ");
            String name = navData.getAttribute("name");
            String info = navData.getTextContent();

            boolean isILSData = Boolean.parseBoolean(navData.getAttribute("isILSData"));
            if (isILSData || formatVersion < 2) {
                String localizerAngDeg = (formatVersion < 3) ? navData.getAttribute("Ang") : navData.getAttribute("localizerAngDeg");
                String glideSlopeAngDeg = (formatVersion < 3) ? String.valueOf(CommandILSManager.ilsApproachAngDeg) : navData.getAttribute("glideSlopeAngDeg");
                double angDeg = Double.parseDouble(localizerAngDeg);
                if (formatVersion == 0) {
                    angDeg = MathHelper.wrapAngleTo180_double(angDeg + 180);
                }

                centerList.put(name, new ILSData(name, Double.parseDouble(posX), Double.parseDouble(posY), Double.parseDouble(posZ), angDeg, Double.parseDouble(glideSlopeAngDeg), new FrequencyID(-1)));
            } else {
                centerList.put(name, new NavRadioData(name, Double.parseDouble(posX), Double.parseDouble(posY), Double.parseDouble(posZ), new FrequencyID(-1)));
            }
        }
        CommandILSManager.centerList = centerList;
    }

    public static void writeILSData(HashMap<String, NavRadioData> centerList) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        assert builder != null;
        DOMImplementation domImpl = builder.getDOMImplementation();

        Document document = domImpl.createDocument("", "Data", null);

        Element tagList = document.getDocumentElement();
        Element elementFileFormat = document.createElement("FileFormat");
        elementFileFormat.setAttribute("Version", "3");
        tagList.appendChild(elementFileFormat);

        Element elementILSList = document.createElement("ILSList");
        //ILSDataの内容を書き込み
        for (Map.Entry<String, NavRadioData> entry : centerList.entrySet()) {
            Element elementILS = document.createElement("ILS");
            NavRadioData data = entry.getValue();
            elementILS.setAttribute("posX", String.valueOf(data.x));
            elementILS.setAttribute("posY", String.valueOf(data.y));
            elementILS.setAttribute("posZ", String.valueOf(data.z));
            elementILS.setAttribute("name", entry.getKey());
            elementILS.appendChild(document.createTextNode(data.name));

            boolean isILSData = data instanceof ILSData;
            elementILS.setAttribute("isILSData", String.valueOf(isILSData));
            if (isILSData) {
                elementILS.setAttribute("localizerAngDeg", String.valueOf(((ILSData) data).localizerAngDeg));
                elementILS.setAttribute("glideSlopeAngDeg", String.valueOf(((ILSData) data).glideSlopeAngDeg));
            }

            elementILSList.appendChild(elementILS);
        }

        tagList.appendChild(elementILSList);

        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        defaultSaveDir.mkdirs();

        File newXML = new File(defaultSaveDir, newDefaultFileName);

        DOMSource source = new DOMSource(document);
        try {
            if (newXML.createNewFile()) {
                FileOutputStream os = new FileOutputStream(newXML);
                StreamResult result = new StreamResult(os);
                try {
                    assert transformer != null;
                    transformer.transform(source, result);
                } catch (TransformerException e) {
                    e.printStackTrace();
                }

                os.flush();
                os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File oldSaveFileName() {
        ServerData data = Minecraft.getMinecraft().func_147104_D();
        String serverName = "";
        if (data != null) {
            serverName = data.serverName + "_";
        }
        return new File(serverName + "ILSList.xml");
    }

    private static final HashMap<String, NavRadioData> emptyData = new HashMap<String, NavRadioData>();

    private static final File defaultSaveDir = new File(DimensionManager.getCurrentSaveRootDirectory(), "AirportMod/");
}
