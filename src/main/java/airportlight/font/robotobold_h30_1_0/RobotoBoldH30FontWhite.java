package airportlight.font.robotobold_h30_1_0;

import airportlight.font.lwjgfont.abstractH30Font;

public class RobotoBoldH30FontWhite extends abstractH30Font {
    public RobotoBoldH30FontWhite() {
        super("robotoboldh30font_0.png");
    }
}

