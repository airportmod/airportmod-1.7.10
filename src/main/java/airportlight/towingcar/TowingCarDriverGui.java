package airportlight.towingcar;

import airportlight.modcore.config.APMKeyConfig;
import airportlight.modcore.gui.overlayGui.OverlayScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;

public class TowingCarDriverGui extends OverlayScreen {
    private EntityTowingCar towingCar;

    public TowingCarDriverGui() {
        super();
        this.setAllowUserInput(true);
    }

    public TowingCarDriverGui(EntityPlayer player) {
        super(player, player.ridingEntity);
        if (player.ridingEntity instanceof EntityTowingCar) {
            this.towingCar = (EntityTowingCar) player.ridingEntity;
            player.rotationYaw = this.towingCar.rotationYaw;
        }
        this.setAllowUserInput(true);
    }

    public void initGui() {
        this.mc.mouseHelper.grabMouseCursor();
    }

    public void drawWorldBackground(int p_146270_1_) {
        if (this.mc.theWorld != null) {
            this.drawGradientRect(this.getWidth() - 100, this.getHeight() - 170, this.getWidth() - 10, this.getHeight() - 40, -804253680, -804253680);
        } else {
            this.drawBackground(p_146270_1_);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        //this.drawGradientRect(this.getWidth() - 100, this.getHeight() - 170, this.getWidth() - 10, this.getHeight() - 40, -804253680, -804253680);
        GameSettings settings = Minecraft.getMinecraft().gameSettings;
        this.fontRendererObj.drawString("Move       : "
                        + Keyboard.getKeyName(settings.keyBindForward.getKeyCode()) + ", "
                        + Keyboard.getKeyName(settings.keyBindBack.getKeyCode()),
                this.getWidth() - 95, this.getHeight() - 160, -1, false);
        this.fontRendererObj.drawString("Strafing    : "
                + Keyboard.getKeyName(settings.keyBindLeft.getKeyCode()) + ", "
                + Keyboard.getKeyName(settings.keyBindRight.getKeyCode()), this.getWidth() - 95, this.getHeight() - 150, -1, false);
        this.fontRendererObj.drawString("Brake      : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake)), this.getWidth() - 95, this.getHeight() - 140, -1, false);
        this.fontRendererObj.drawString("Gate Open  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Open)), this.getWidth() - 95, this.getHeight() - 120, -1, false);
        this.fontRendererObj.drawString("PickUp     : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_PickUp)), this.getWidth() - 95, this.getHeight() - 110, -1, false);
        this.fontRendererObj.drawString("Release    : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Release)), this.getWidth() - 95, this.getHeight() - 100, -1, false);
        this.fontRendererObj.drawString("Gate Close : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Close)), this.getWidth() - 95, this.getHeight() - 90, -1, false);
        this.fontRendererObj.drawString("Cam Reset  : " + Keyboard.getKeyName(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)), this.getWidth() - 95, this.getHeight() - 70, -1, false);
        this.fontRendererObj.drawString("Dismount   : " + Keyboard.getKeyName(settings.keyBindSneak.getKeyCode()), this.getWidth() - 95, this.getHeight() - 60, -1, false);
    }


    @Override
    protected void keyTyped(char p_73869_1_, int p_73869_2_) {
        if (Keyboard.getEventKeyState()) {
            int keyID = Keyboard.getEventKey();

            if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_PickUp)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.PickUp.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Release)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.Release.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Open)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.GateOpen.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Towing_Close)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.GateAction, GateAction.GateClose.modeID);
            } else if (keyID == APMKeyConfig.getKeyidFromName(APMKeyConfig.str_CameraReset)) {
                this.towingCar.DataFlagUpdateSync(TowingDataFlag.CameraReset, 1);
            }
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (Keyboard.isKeyDown(APMKeyConfig.getKeyidFromName(APMKeyConfig.str_Brake))) {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Brake, 1);
        } else {
            this.towingCar.DataFlagUpdateSync(TowingDataFlag.Brake, 0);
        }
    }

    public boolean doesGuiPauseGame() {
        return false;
    }
}
