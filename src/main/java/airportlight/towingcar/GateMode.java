package airportlight.towingcar;

public enum GateMode {
    Closed(0), Closing(1), PickUp(2), LiftUp(3), CanRun(4), LiftDown(5), Opening(6), Opened(7);

    private static GateMode[] values;
    public final int modeID;

    GateMode(int modeID) {
        this.modeID = modeID;
    }

    static GateMode getModeFromNum(int modeNum) {
        if (values == null) {
            values = new GateMode[]{Closed, Closing, PickUp, LiftUp, CanRun, LiftDown, Opening, Opened};
        }
        return values[modeNum];
    }
}
