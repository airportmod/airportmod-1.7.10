package airportlight.towingcar;

public enum TowingDataFlag {
    GuiOpen(0, 10),
    GateAction(1, 11),
    Brake(2, 12),
    Dismount(3, 13),
    GateMode(4, 14),
    CameraReset(5, 15);

    private static TowingDataFlag[] values;
    public final int flagID;
    public final int memberID;

    TowingDataFlag(int memberID, int flagID) {
        this.memberID = memberID;
        this.flagID = flagID;
    }

    static TowingDataFlag getFlagFromMemberID(int memberID) {
        if (values == null) {
            values = new TowingDataFlag[]{GuiOpen, GateAction, Brake, Dismount, GateMode, CameraReset};
        }
        return values[memberID];
    }
}
