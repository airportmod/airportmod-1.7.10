package airportlight.towingcar;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

import java.util.List;

public class CommandTowingCar extends CommandBase {
    @Override
    public String getCommandName() {
        return "TowingCar";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "APM-TowingCar:commandUsage.";
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] args) {
        if (args.length > 0) {
            if ("SearchRange".equals(args[0])) {
                if (args.length == 2) {
                    TowingCarSetting.SearchRange = Double.parseDouble(args[1]);
                }
                p_71515_1_.addChatMessage(new ChatComponentText("TowingCar SearchRange : " + TowingCarSetting.SearchRange));
            } else if ("WheelBase".equals(args[0])) {
                if (args.length == 2) {
                    TowingCarSetting.WheelBase = Double.parseDouble(args[1]);
                }
                p_71515_1_.addChatMessage(new ChatComponentText("TowingCar WheelBase : " + TowingCarSetting.WheelBase));
            } else if ("?".equals(args[0])) {
                p_71515_1_.addChatMessage(new ChatComponentText("§2-----APM Command Help-----"));
                p_71515_1_.addChatMessage(new ChatComponentText("/TowingCar SearchRange [range] -> TowingCar SerchRange for PickUpEntity."));
                p_71515_1_.addChatMessage(new ChatComponentText("/TowingCar WheelBase [long] -> PickUpEntity's WheelBase. Affects the turn rate of PickUpEntity."));
            }
        }
    }

    @Override
    public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
        if (p_71516_2_.length == 1) {
            return getListOfStringsMatchingLastWord(p_71516_2_, "SearchRange", "WheelBase", "?");
        }
        return null;
    }
}
