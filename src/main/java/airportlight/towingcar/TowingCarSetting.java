package airportlight.towingcar;

import java.util.ArrayList;
import java.util.Arrays;

public class TowingCarSetting {
    public static double SearchRange = 6;
    public static double WheelBase = 10;
    public static ArrayList<String> pickUpIgnore = new ArrayList<String>(Arrays.asList("mcheli.MCH.E.Seat", "mcheli.MCH.E.PSeat"));
}
