# Airport Mod For Minecraft-1.7.10

[![Discord](https://img.shields.io/discord/795872924615442462.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/bGVr3PpSWK)

This mod adds equipment and equipment related to airfields and airplanes, such as airfield lights and ILS.

## Download Minecraft Mod
Curse Forge [https://www.curseforge.com/minecraft/mc-mods/airportmod](https://www.curseforge.com/minecraft/mc-mods/airportmod)

## Copyright Notice

(c) 2021 kuma_code, noyciy7037, Alice and other contributors

## License

This Software is released under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).



## For 1.12.2 Version
[https://gitlab.com/airportmod/airportmod-1.12.2](https://gitlab.com/airportmod/airportmod-1.12.2)

## For 1.16.5 Version
[https://gitlab.com/airportmod/airportmod-1.16.5](https://gitlab.com/airportmod/airportmod-1.16.5)
