# Airport Mod For Minecraft-1.7.10

[![Discord](https://img.shields.io/discord/795872924615442462.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/bGVr3PpSWK)


このMODでは、飛行場灯やILSなど、飛行場や飛行機に関連する設備・機器を追加します。

## コピーライト

(c) 2021 kuma_code, noyciy7037, Alice and other contributors

## ライセンス

本ソフトウェアは、[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)に基づいてリリースされています。
